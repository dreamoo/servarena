<?php
// customize ACF path
add_filter('acf/settings/path', 'my_acf_settings_path');
 
function my_acf_settings_path( $path ) {
 
    // update path
    $path = get_stylesheet_directory() . '/panel/';
    
    // return
    return $path;
    
}
// customize ACF dir
add_filter('acf/settings/dir', 'my_acf_settings_dir');
 
function my_acf_settings_dir( $dir ) {
 
    // update path
    $dir = get_stylesheet_directory_uri() . '/panel/';
    
    // return
    return $dir;
    
}
 

// Hide ACF field group menu item
add_filter('acf/settings/show_admin', '__return_false');



add_filter('acf/settings/save_json', 'my_acf_json_save_point');
 
function my_acf_json_save_point( $path ) {
    
    // update path
    $path = get_stylesheet_directory() . '/panel/json';
    
    
    // return
    return $path;
    
}

add_filter('acf/settings/load_json', 'my_acf_json_load_point');

function my_acf_json_load_point( $paths ) {
    
    // remove original path (optional)
    unset($paths[0]);
    
    
    // append path
    $paths[] = get_stylesheet_directory() . '/panel/json';
    
    
    // return
    return $paths;
    
}



add_action('acf/init', 'my_acf_init');

function my_acf_init() {
	
  if( function_exists('acf_add_options_page') ) {

//      acf_add_options_page(array(
//          'page_title' 	=> 'Theme General Settings',
//          'menu_title'	=> 'اعدادات متقدمه',
//          'menu_slug' 	=> 'theme-general-settings',
//          'capability'	=> 'edit_posts',
//          'redirect'		=> false
//      ));

//      acf_add_options_sub_page(array(
//          'menu_title'	=> 'الهيدر',
//          'menu_slug' 	=> 'header',
//          'post_id' => 'ar',
//          'parent_slug'	=> 'theme-general-settings',
//      ));
//      acf_add_options_sub_page(array(
//          'menu_title'	=> 'Header English',
//          'menu_slug' 	=> 'header-en',
//          'post_id' => 'en',
//          'parent_slug'	=> 'theme-general-settings-hideen',
//      ));

//      acf_add_options_sub_page(array(
//          'page_title' 	=> 'Theme Footer Settings',
//          'menu_title'	=> 'Footer',
//          'parent_slug'	=> 'theme-general-settings',
//      ));

//    
//foreach (['en', 'ar'] as $lang) {
//
//    acf_add_options_sub_page([
//        'page_title' => "Page name ${$lang}",
//        'menu_title' => __("Page name ${$lang}", 'textdomain'),
//        'menu_slug' => "page-name-${lang}",
//        'post_id' => $lang,
//        'parent' => 'theme-general-settings'
//    ]);
//
//}
    
  }
	
}

//  Include ACF
include_once( get_stylesheet_directory() . '/panel/acf.php' );

