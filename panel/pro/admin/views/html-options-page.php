<div class="wrap acf-settings-wrap">
	
	<h1><?php echo $page_title; ?></h1>
	
	<form id="post" method="post" name="post">
		
		<?php 
		
		// render post data
		acf_form_data(array(
			'screen'	=> 'options',
			'post_id'	=> $post_id,
		));
		
		wp_nonce_field( 'meta-box-order', 'meta-box-order-nonce', false );
		wp_nonce_field( 'closedpostboxes', 'closedpostboxesnonce', false );
		?>
		
		<div id="poststuff">
			
			<div id="post-body" class="columns-1">
<!--
                <?php $current_screen = get_current_screen();
                  if ($current_screen->id == 'admin_page_header-en') :?>
                      <a href="<?php echo admin_url('admin.php?page=header');?>">Arabic</a>
                  <?php elseif($current_screen->id == '%d8%a7%d8%b9%d8%af%d8%a7%d8%af%d8%a7%d8%aa-%d9%85%d8%aa%d9%82%d8%af%d9%85%d9%87_page_header') :?>
                      <a href="<?php echo admin_url('admin.php?page=header-en');?>">English</a>
                  <?php endif;?>
-->
				<div id="postbox-container-2" class="postbox-container">
					
					<?php do_meta_boxes('acf_options_page', 'normal', null); ?>
					
				</div>
				<div id="postbox-container-1" class="postbox-container">
					
					<?php do_meta_boxes('acf_options_page', 'side', null); ?>
						
				</div>
			
			</div>
			
			<br class="clear">
		
		</div>
		
	</form>
	
</div>