<!doctype html>
<html <?php language_attributes(); ?> class="no-js">
	<head>
		<meta charset="<?php bloginfo('charset'); ?>">
		<title><?php wp_title(''); ?><?php if(wp_title('', false)) { echo ' :'; } ?> <?php bloginfo('name'); ?></title>

		<link href="//www.google-analytics.com" rel="dns-prefetch">
        <meta name="google" content="notranslate">

        <link href="<?php echo get_template_directory_uri(); ?>/assets/img/favicon.ico" rel="shortcut icon">
        <link href="<?php echo get_template_directory_uri(); ?>/assets/css/bootstrap.min.css" rel="stylesheet">
        <?php if(is_rtl()):?>
          <link href="<?php echo get_template_directory_uri(); ?>/assets/css/bootstrap-rtl.min.css" rel="stylesheet">
        <?php endif;?>
        <link href="<?php echo get_template_directory_uri(); ?>/assets/slick/slick.css" rel="stylesheet">
        <link href="<?php echo get_template_directory_uri(); ?>/assets/css/font-awesome.min.css" rel="stylesheet">
        <link href="<?php echo get_template_directory_uri(); ?>/assets/css/aos.css" rel="stylesheet">
        <link href="<?php echo get_template_directory_uri(); ?>/assets/css/animate.min.css" rel="stylesheet">
<!--
        <link href="<?php echo get_template_directory_uri(); ?>/assets/css/fontawesome-all.min.css" rel="stylesheet">
        <link href="<?php echo get_template_directory_uri(); ?>/assets/css/fontawesome-5.2.0.min.css" rel="stylesheet">
-->
        <link href="<?php echo get_template_directory_uri(); ?>/assets/css/fontawesome-5.3.1.min.css" rel="stylesheet">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="<?php bloginfo('description'); ?>">
     

		<?php wp_head(); ?>
	</head>
	<body <?php body_class(); ?>>
        <main>
<header class="navbar-fixed-top<?php echo (!is_front_page())?' inside_page':'';?>">
<nav class="navbar navbar-default<?php echo (!is_front_page())?' nav-inside':'';?>">
  <div class="container">
	  <div class="row">
		  <div class="col-md-3">
			<div class="navbar-header">
			  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			  </button>
				<h1 id="logo">
					<a class="navbar-brand" href="<?php echo home_url();?>"><?php _e('servarena','mdwp');?></a>
				</h1>
			</div>
		  </div>
		  <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
			<div class="col-md-7">
			  <ul class="nav navbar-nav">
                <?php $locations = get_nav_menu_locations();
                // if there's a location for the primary menu
                if ( isset( $locations['header-menu']) ) {
                    $menu = get_term( $locations['header-menu'], 'nav_menu');
                    // if there are items in the primary menu
                    if ( $items = wp_get_nav_menu_items( $menu->name ) ) {
                        // loop through all menu items to display their content
                        foreach ( $items as $item ) {
                            // if the current item is not a top level item, skip it
                            if ($item->menu_item_parent != 0) {
                                continue;
                            }
                            // get the ID of the current nav item
                            $curNavItemID = $item->ID;
                            $current = ( $item->object_id == get_queried_object_id() ) ? 'active' : '';
                            // get the custom classes for the item
                            // (determined within the WordPress Appearance > Menu section)
                            $classList = implode(" ", $item->classes);
                            echo "<li class=\"{$current} {$classList}\">";
                            // build the mega-menu
                            // if 'mega-menu'  exists within the class
                            if ( in_array('mega-menu', $item->classes)) {
								echo "<a href=\"{$item->url}\" class=\"dropdown-toggle\" data-toggle=\"dropdown\" role=\"button\" aria-haspopup=\"true\" aria-expanded=\"false\">{$item->title}</a>";?>
                              <ul class="dropdown-menu multi-column columns-3">
                                  <div class="row">
                                    
                                      <?php  foreach ($items as $itemshead) {
                                        if ( $itemshead->menu_item_parent == $curNavItemID ){
                                            $itemsheadarr[] = $itemshead;
                                          }
                                        }?>
                                      <?php foreach ($itemsheadarr as $head) :?>
                                        <?php if ( in_array('mega-info', $head->classes)) :?>
                                        <div class="col-md-4 col-xs-12">
                                            <ul class="multi-column-dropdown">
                                              <li class="none">
                                                 <i class="fas fa-phone-volume phone-volume fa-lg"></i>
                                                 <span class="title"><?php echo $head->title;?></span>
                                              </li>
                                              <li><a href="#"><?php echo $head->description;?></a></li>
                                              <li class="none"><a class="tel" href="#"> 920020332</a></li>
                                            </ul>
                                        </div>
                                        <?php else:?>
                                         <div class="col-md-4 col-xs-12">
                                              <ul class="multi-column-dropdown">
                                                <li class="none">
                                                   <i class="fas <?php echo $head->description;?> fa-lg" aria-hidden="true"></i>
                                                   <span class="title"><?php echo $head->title;?></span>
                                                </li>
                                                <?php foreach ($items as $headsubs) :
                                                  if ( $headsubs->menu_item_parent == $head->ID ){
                                                    echo "<li><a href=\"{$headsubs->url}\">{$headsubs->title}</a></li>";
                                                  }
                                                endforeach;?>
                                              </ul>
                                          </div>
                                        <?php endif;?>
                                      <?php endforeach; ?>
                                  </div>
                              </ul>

                            <?php }else{
								echo "<a class=\"{$current} {$classList}\" href=\"{$item->url}\">{$item->title}</a>";
							}
                            echo '</li>';
                        }
                    }
                } ?>
			  </ul>
			  </div>
			<div class="col-md-2">

			  <ul class="nav navbar-nav">
				  <?php pll_the_languages(array('hide_current'=>1));?>
			  </ul>

			</div>
		</div>
	  </div>
  </div>
</nav>
</header>