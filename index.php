<?php get_header(); ?>

<main role="main" class="blog">
<?php $count = 0 ;
if (have_posts()): while (have_posts()) : the_post();$count++;
$cats = get_the_category(); $cat_name = $cats[0]->name;
if($count == 1): 
	$featured_img_url =  wp_get_attachment_url( get_post_thumbnail_id($post->ID), 'thumbnail' );
	?>

	<section class="last-post">
		<div class="post-img" style="background-image:url(<?php echo $featured_img_url ; ?>)">
			<?php /*the_post_thumbnail('full');*/ ?>
		</div>
		<div class="post-data" data-aos="fade-down" data-aos-once="true">
			<div class="container">
				<h2>
					<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a>
				</h2>
				<?php mdwpwp_excerpt('mdwpwp_index');?>
			</div>
		</div>
	</section>

<?php endif;endwhile;endif; ?>

</main>
<section class="posts container">

			<?php $count2 = 0; if (have_posts()): while (have_posts()) : the_post();$count2++;
			$cats = get_the_category(); $cat_name = $cats[0]->name;
			if($count2 > 1): ?>

			<article id="post-<?php the_ID(); ?>" <?php post_class('col-xs-12 col-md-6'); ?>>
			  <div class="media" data-aos="fade-left" data-aos-delay="100" data-aos-once="true">
			  	<div class="media-left" data-aos="fade-left" data-aos-delay="400" data-aos-once="true">
					<!-- post thumbnail -->
					<?php if ( has_post_thumbnail()) : // Check if thumbnail exists ?>
						<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
							<?php the_post_thumbnail(array(120,120), array('class' => 'media-object')); // Declare pixel size you need inside the array ?>
						</a>
					<?php endif; ?>
					<!-- /post thumbnail -->
				</div>

				<div class="media-body" data-aos="fade-left" data-aos-delay="600"  data-aos-once="true">
					<h4 class="media-heading">
						<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a>
					</h4>
					<div class="post-details">
						<span class="category"><?php echo $cat_name;?></span> | 
						<span class="date"><?php the_time('F Y'); ?> </span>
					</div>
					<?php mdwpwp_excerpt('mdwpwp_index');?>

				</div>


			  </div>
			</article>

			<?php endif;endwhile;endif; ?>

		<?php get_template_part('pagination'); ?>

	</section>

<?php get_sidebar(); ?>

<?php get_footer(); ?>
