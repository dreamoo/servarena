<?php get_header(); ?>

<?php $year_arr = array();
  if (have_posts()): while (have_posts()) : the_post(); 
  $get_year = get_the_date( 'Y' );
  if(!in_array($get_year,$year_arr))
    $year_arr[]= $get_year;
endwhile;endif;?>


  <main id="innerpage" role="main">
    <section class="section mediacenter">
      <div class="container">
        <div class="controls">
            <button type="button" class="control" data-filter="all">All</button>
            <?php foreach($year_arr as $yearitem):?>
            <button type="button" class="control" data-filter=".year<?php echo $yearitem;?>"><?php echo $yearitem;?></button>
            <?php endforeach;?>
        </div>
        <div class="catgallery">
          <div class="row">
            <?php if (have_posts()): while (have_posts()) : the_post();
              $opts = get_post_meta( $post->ID, '_post_opts', true );
              if(!$opts) 
              $opts = array();
            ?>
            
            <?php $gl_imgs = explode( ',', $opts['gallery'] );
            if(is_array($gl_imgs) && !empty($gl_imgs[0])):
            foreach($gl_imgs as $gl_img):?>
                      <div id="post-<?php the_ID(); ?>" class="col-12 col-md-4 mix year<?php echo get_the_date( 'Y' );?>">
                        <img src="<?php echo wp_get_attachment_image_src( $gl_img, array(700,700) )[0]?>" class="responsive-img" alt="" />
                      </div>
              <?php endforeach;endif;?>

              <?php endwhile;endif; ?>
            </div>
          </div>
        </div>
      </section>
	</main>

        
        <script>
            var containerEl = document.querySelector('.catgallery');

            var mixer = mixitup(containerEl);
        </script>
<?php get_sidebar(); ?>

<?php get_footer(); ?>
