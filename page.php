<?php get_header(); ?>

<?php if (have_posts()): while (have_posts()) : the_post(); ?>

<?php  // ====== Layout Start
if( have_rows('page_builder') ):
while ( have_rows('page_builder') ) : the_row();?>

  
  <?php if( get_row_layout() == 'header' ):
      include( locate_template( 'templates/blocks/block-header.php', false, false ) )?>

  <?php elseif( get_row_layout() == 'nav_list' ):
      include( locate_template( 'templates/blocks/block-navlist.php', false, false ) )?>

  <?php elseif( get_row_layout() == 'advantages' ):
      include( locate_template( 'templates/blocks/block-advantages.php', false, false ) )?>

  <?php elseif( get_row_layout() == 'server_plans' ):
      include( locate_template( 'templates/blocks/block-serverplans.php', false, false ) )?>

  <?php elseif( get_row_layout() == 'block_why' ):
      include( locate_template( 'templates/blocks/block-why.php', false, false ) )?>

  <?php elseif( get_row_layout() == 'block_about' ):
      include( locate_template( 'templates/blocks/block-about.php', false, false ) )?>

  <?php elseif( get_row_layout() == 'block_start' ):
      include( locate_template( 'templates/blocks/block-start.php', false, false ) )?>

  <?php elseif( get_row_layout() == 'block_domains' ):
      include( locate_template( 'templates/blocks/block-domains.php', false, false ) )?>

  <?php elseif( get_row_layout() == 'block_clientarea' ):
      include( locate_template( 'templates/blocks/block-clientarea.php', false, false ) )?>

  <?php elseif( get_row_layout() == 'sectors' ):
      include( locate_template( 'templates/blocks/block-sectors.php', false, false ) )?>

  <?php elseif( get_row_layout() == 'faq' ):
      include( locate_template( 'templates/blocks/block-faq.php', false, false ) )?>
      
  <?php elseif( get_row_layout() == 'block_payment' ):
      include( locate_template( 'templates/blocks/block-payment.php', false, false ) )?>

  <?php elseif( get_row_layout() == 'block_content' ):
      include( locate_template( 'templates/blocks/block-content.php', false, false ) )?>

  <?php elseif( get_row_layout() == 'block_tabs' ):
      include( locate_template( 'templates/blocks/block-tabs.php', false, false ) )?>

  <?php elseif( get_row_layout() == 'block_slider' ):
      include( locate_template( 'templates/blocks/block-slider.php', false, false ) )?>

  <?php elseif( get_row_layout() == 'block_works' ):
      include( locate_template( 'templates/blocks/block-works.php', false, false ) )?>

  <?php elseif( get_row_layout() == 'block_contactus' ):
      include( locate_template( 'templates/blocks/block-contactus.php', false, false ) )?>

  <?php elseif( get_row_layout() == 'block_servertabs' ):
      include( locate_template( 'templates/blocks/block-servertabs.php', false, false ) )?>

  <?php endif; ?>


<?php endwhile;endif;?>


<?php endwhile;endif; ?>

<?php get_footer(); ?>
