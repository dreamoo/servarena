<?php /* Template Name: Mobile */ get_header();?>
<div class="slider_insidepage">
    <section class="app_slide slider">
        <div class="inside-content">
          <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <div class="slide-text">
                            <h2><?php echo the_field('header_title');?></h2>
                            <p><?php  echo the_field('header_description');?></p>
                            <?php if(!empty(get_field('header_btn_more'))):?>
                            <a class="btn btn-price" href="<?php echo the_field('header_btn_more');?>"><?php _e('order now','mdwp');?> </a>
                            <?php endif;?>
                        </div>
                    </div>
                    <div class="col-md-5 col-sm-6 col-xs-12">
                        <div class="slide-img">
                            <img src="<?php echo the_field('header_image');?>"/>
                        </div>
                    </div>
                </div>
            </div>
          </div>
        </div>
    </section>
</div>

<section class="step_bulid_app text-center">
   <div class="container">
	   <div class="row" data-aos="fade-down" data-aos-once="true">
		   <div class="col-md-12">
				<div class="step-title">
					<h3><?php echo the_field('steps_title');?></h3>
				</div>
		   </div>
		   <div class="col-md-offset-2 col-md-8">
			   <div class="step-text">
				<p><?php echo the_field('steps_desc');?></p>
			   </div>
		   </div>
		</div>
   </div>
   <div class="diagram_steps">
	   <div class="tube_channel diagram tube"></div>
	   <div class="first_circle diagram tube"></div>
	   <div class="second_circle diagram tube"></div>
	   <div class="third_circle diagram tube"></div>
	   <div class="last_circle diagram tube"></div>
	   <div class="final_circle diagram tube"></div>
	   <div class="mobile_app diagram tube"></div>

	   <?php $steps_class_arr = ['first_text','sec_text','third_text','fourth_text','last_text'];$count=0;
	    if( have_rows('steps_items') ): while ( have_rows('steps_items') ) : the_row();?>
	   <div data-aos="fade-down" data-aos-once="true" class="col-md-2 col-sm-2 col-xs-4 diagram <?php echo $steps_class_arr[$count];?>">
		   <div class="steps_text text-right">
				<h2>0<?php echo $count+1;?></h2>
			   <h5><?php the_sub_field('title');?></h5>
			   <h6><?php the_sub_field('desc');?></h6>
		   </div>
	   </div>
	   <?php $count++;endwhile;endif;?>

	   <div class="end_trigger"></div>
   </div>
</section>
<!-- ======================mobile_app============================ -->
 <section class="mobile_app text-center">
	 <div class="container">
		<div class="row" data-aos="fade-down" data-aos-once="true" data-aos-delay="300">
		<?php if( get_field('android') ):?>
			<div class="col-md-4 col-sm-6 col-xs-12 app_content" data-aos="fade-down" data-aos-once="true" data-aos-delay="400">
				<i class="fab fa-android"></i>
				<h4><?php _e('android','mdwp');?></h4>
				<p><?php echo get_field('android_desc');?></p>
				<?php if(!empty(get_field('android_link'))):?>
				<a class="btn btn-price price-prop" href="<?php echo get_field('windows_link');?>"><?php _e('order now','mdwp');?> </a>
				<?php endif;?>
			</div>
		<?php endif;?>
		<?php if( get_field('ios') ):?>
			<div class="col-md-4 col-sm-6 col-xs-12 app_content" data-aos="fade-down" data-aos-once="true" data-aos-delay="500">
				<i class="fab fa-apple"></i>
				<h4>IOS</h4>
				<p><?php echo get_field('ios_desc');?></p>
				<?php if(!empty(get_field('ios_link'))):?>
				<a class="btn btn-price price-prop" href="<?php echo get_field('ios_link');?>"><?php _e('order now','mdwp');?> </a>
				<?php endif;?>
			</div>
		<?php endif;?>
		<?php if( get_field('windows') ):?>
			<div class="col-md-4 col-sm-12 col-xs-12 app_content" data-aos="fade-down" data-aos-once="true" data-aos-delay="600">
				<i class="fab fa-windows"></i>
				<h4><?php _e('Windows phone','mdwp');?></h4>
				<p><?php echo get_field('windows_desc');?></p>
				<?php if(!empty(get_field('windows_link'))):?>
				<a class="btn btn-price price-prop" href="<?php echo get_field('windows_link');?>"><?php _e('order now','mdwp');?> </a>
				<?php endif;?>
			</div>
		<?php endif;?>
		 </div>
	 </div>
 </section>
 <!-- ======================our_work============================ -->
  <section class="our_work">
	<div class="container">
		<div class="row">
			<div class="col-md-12"  data-aos="fade-down" data-aos-once="true">
				<div class="main-title">
					<div class="title-group">
						<?php echo get_field('works_head');?>
					</div>
				</div>
			</div>
		</div>
		<div class="dalil_app">
		   <div class="dalil_logo dalil_control"></div>
		   <div class="dalil_bg dalil_control"></div>
		   <div class="dalil_element">
				<div class="elem1 dalil_control"></div>
				<div class="elem2 dalil_control"></div>
				<div class="elem3 dalil_control"></div>
				<div class="elem4 dalil_control"></div>
				<div class="elem5 dalil_control"></div>
				<div class="elem6 dalil_control"></div>
				<div class="elem7 dalil_control"></div>
				<div class="elem8 dalil_control"></div>
			</div>
			<div class="dalil_title dalil_control">
				<h2> <?php echo get_field('app_title');?></h2>
			</div>
		</div>
		<div class="row what_dalil">
			<div class="col-md-6 col-sm-5 col-xs-12">
				<?php $description = explode("\n", get_field('app_desc'), 2);
					echo '<h5 class="app_title">'.$description[0].'</h5>';
					echo '<p class="app_content">'.$description[1].'</p>';
				?>
			</div>
		</div>
		<div class="row tech_uses">
			<div class="col-md-12">
				<h5 class="app_title"><?php echo get_field('tech_uses_title');?> </h5>
			</div>
			<ul class="app_content">
				<?php 
					$bits = explode("\n", get_field('tech_uses_desc'));
					foreach($bits as $bit){
						echo "<li>" . $bit . "</li>";
					}
				?> 
			</ul>
		</div>
		<div class="smart_phone">
			<div class="smart_phone1 smart_prop"></div>
			<div class="smart_phone2 smart_prop"></div>
			<div class="smart_phone3 smart_prop"></div>
			<div class="smart_phone4 smart_prop"></div>
			<div class="smart_phone5 smart_prop"></div>
			<div class="smart_phone6 smart_prop"></div>
			<div class="smart_phone7 smart_prop"></div>
			<div class="smart_phone8 smart_prop"></div>
		</div>
		<div class="counter_app text-center">
			<div class="row">
			<?php $statistics = get_field('statistics');?>
				<?php if(!empty($statistics['downloads'])):?>
				<div class="col-md-2 col-sm-3 col-xs-4">
					<h2><?php echo $statistics['downloads'];?></h2>
					<span><?php _e('Downloads from store','mdwp');?></span>
				</div>
				<?php endif;?>
				<?php if(!empty($statistics['user'])):?>
				<div class="col-md-2 col-sm-3 col-xs-4">
					<h2><?php echo $statistics['user'];?></h2>
					<span><?php _e('Users','mdwp');?></span>
				</div>
				<?php endif;?>
				<?php if(!empty($statistics['numbers'])):?>
				<div class="col-md-2 col-sm-3 col-xs-4">
					<h2><?php echo $statistics['numbers'];?></h2>
					<span><?php _e('Mobile numbers','mdwp');?></span>
				</div>
				<?php endif;?>
			</div>
		</div>
		<div class="app_store">
			<div class="row">
			<?php if(!empty($statistics['google_link'])):?>
				<div class="col-md-2 col-sm-6 col-xs-6">
					<a href="<?php echo $statistics['google_link'];?>">
						<img src="<?php echo get_template_directory_uri(); ?>/assets/img/android-app-store.png"/>
					</a>
				</div>
			<?php endif;?>
			<?php if(!empty($statistics['apple_link'])):?>
				<div class="col-md-2 col-sm-6 col-xs-6">
					<a href="<?php echo $statistics['apple_link'];?>">
						<img src="<?php echo get_template_directory_uri(); ?>/assets/img/appstore.png"/>
					</a>
				</div>
			<?php endif;?>
			</div>
		</div>
		<div class="end_trigger1"></div>
	 </div>
	   <div id="trigger" ></div>
 </section>


 <?php  // ====== Layout Start
if( have_rows('page_builder') ):
while ( have_rows('page_builder') ) : the_row();?>

  
  <?php if( get_row_layout() == 'header' ):
      include( locate_template( 'templates/blocks/block-header.php', false, false ) )?>

  <?php elseif( get_row_layout() == 'nav_list' ):
      include( locate_template( 'templates/blocks/block-navlist.php', false, false ) )?>

  <?php elseif( get_row_layout() == 'advantages' ):
      include( locate_template( 'templates/blocks/block-advantages.php', false, false ) )?>

  <?php elseif( get_row_layout() == 'server_plans' ):
      include( locate_template( 'templates/blocks/block-serverplans.php', false, false ) )?>

  <?php elseif( get_row_layout() == 'block_why' ):
      include( locate_template( 'templates/blocks/block-why.php', false, false ) )?>

  <?php elseif( get_row_layout() == 'block_about' ):
      include( locate_template( 'templates/blocks/block-about.php', false, false ) )?>

  <?php elseif( get_row_layout() == 'block_start' ):
      include( locate_template( 'templates/blocks/block-start.php', false, false ) )?>

  <?php elseif( get_row_layout() == 'block_domains' ):
      include( locate_template( 'templates/blocks/block-domains.php', false, false ) )?>

  <?php elseif( get_row_layout() == 'block_clientarea' ):
      include( locate_template( 'templates/blocks/block-clientarea.php', false, false ) )?>

  <?php elseif( get_row_layout() == 'sectors' ):
      include( locate_template( 'templates/blocks/block-sectors.php', false, false ) )?>

  <?php elseif( get_row_layout() == 'faq' ):
      include( locate_template( 'templates/blocks/block-faq.php', false, false ) )?>
      
  <?php elseif( get_row_layout() == 'block_payment' ):
      include( locate_template( 'templates/blocks/block-payment.php', false, false ) )?>

  <?php elseif( get_row_layout() == 'block_content' ):
      include( locate_template( 'templates/blocks/block-content.php', false, false ) )?>

  <?php elseif( get_row_layout() == 'block_tabs' ):
      include( locate_template( 'templates/blocks/block-tabs.php', false, false ) )?>

  <?php elseif( get_row_layout() == 'block_slider' ):
      include( locate_template( 'templates/blocks/block-slider.php', false, false ) )?>

  <?php elseif( get_row_layout() == 'block_works' ):
      include( locate_template( 'templates/blocks/block-works.php', false, false ) )?>

  <?php elseif( get_row_layout() == 'block_contactus' ):
      include( locate_template( 'templates/blocks/block-contactus.php', false, false ) )?>

  <?php elseif( get_row_layout() == 'block_servertabs' ):
      include( locate_template( 'templates/blocks/block-servertabs.php', false, false ) )?>

  <?php endif; ?>


<?php endwhile;endif;?>

<?php 
$block_statistics = get_field('block_statistics');
if($block_statistics['active_block']):?>
<section class="statistics">
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="main-title">
				<div class="title-group" data-aos="fade-down" data-aos-once="true">
					<?php echo $block_statistics['header'];?>
				</div>
			</div>
		</div>
	</div>
	<div class="statistic_content">
		<div class="row text-center" data-aos="fade-down" data-aos-once="true" data-aos-delay="400">
			<div class="col-md-3 col-sm-6 col-xs-12 statis_content">
				<div class="statis-img">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/handshack.png" alt="clients"/>
				</div>
				<div class="statis-desc statis1">
					<h4><span class="counter-num"><?php echo $block_statistics['client'];?></span></h4>
					<span class="statis-title"><?php _e('Client','mdwp');?></span>
				</div>
			</div>
			<div class="col-md-3 col-sm-6 col-xs-12 statis_content">
				<div class="statis-img">
					<img src="<?php echo get_template_directory_uri(); ?>/assets/img/buliding.png" alt="app"/>
				</div>
				<div class="statis-desc statis2">
					<h4><span class="counter-num"><?php echo $block_statistics['app'];?></span></h4>
					<span class="statis-title"><?php _e('App','mdwp');?></span>
				</div>
			</div>
			<div class="col-md-3 col-sm-6 col-xs-12 statis_content">
				<div class="statis-img">
					<img src="<?php echo get_template_directory_uri(); ?>/assets/img/user.png" alt="users"/>
				</div>
				<div class="statis-desc statis3">
					<h4><span class="counter-num"><?php echo $block_statistics['user'];?></span></h4>
					<span  class="statis-title"><?php _e('User','mdwp');?></span>
				</div>
			</div>
			<div class="col-md-3 col-sm-6 col-xs-12 statis_content">
				<div class="statis-img">
					<img src="<?php echo get_template_directory_uri(); ?>/assets/img/global.png" alt="workers"/>
				</div>
				<div class="statis-desc statis4">
                    <h4><span class="counter-num"><?php echo $block_statistics['employee'];?></span></h4>
					<span  class="statis-title"><?php _e('Employee','mdwp');?></span>
				</div>
			</div>
		</div>
		</div>
	</div>
</section>
<?php endif;?>
<?php get_footer(); ?>