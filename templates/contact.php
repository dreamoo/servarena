<?php /* Template Name: Contact */ get_header();?>

<section id="map"></section>
<div class="map-title">
      <div class="row">
        <div class="col-md-3">
            <div class="main-title">
                <div class="title-group">
                    <h2><?php if (get_locale() == 'en_US') {echo "Branches";}else{echo "فروعنا";}?></h2>
                </div>
            </div>
          </div>
      </div>
</div>
<!-- ======================location============================ -->  
<section class="locations">
    <div class="countries">
        <div class="container">
            <div class="row">
                
                <?php $offices = get_field('offices');
                 if(is_array($offices)): foreach($offices as $office):
                    $office_icon = $office['icon'];
                    if (empty($office_icon))
                        $office_icon = get_template_directory_uri()."/assets/img/saudi-arabia.png";
                ?>
                  <div class="col-md-3 col-md-offset-1 col-sm-4 col-xs-6 contact_height">
                    <div class="loc-img">
                        <img data-aos="fade-left" data-aos-delay="100" data-aos-once="true" src="<?php echo $office_icon;?>"/>
                    </div>
                    <div class="loc-text" data-aos="fade-left" data-aos-delay="400" data-aos-once="true">
                        <h5 class="loc-title"><?php echo $office['country'];?></h5>
                        <ul class="locate">
                        <?php 
                            $bits = explode("\n", $office['details']);
                            foreach($bits as $bit){
                                echo "<li>" . $bit . "</li>";
                            }
                        ?> 
                        </ul>
                    </div>
                  </div>
                <?php endforeach;endif;?>
            </div>
        </div>
    </div>
    <div class="contact_ltr"><div data-aos="fade-left" class="cloud1"></div></div>
    <div class="contact_ltr"><div data-aos="fade-right" class="cloud2"></div></div>
    <div class="contact_ltr"><div data-aos="fade-right" class="person"></div></div>
</section>
  <!-- ======================contact============================ -->
    <section class="contact_campany">
        <div class="countries">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="contact-text text-center" data-aos="fade-left" data-aos-once="true">
                            <?php echo get_field('contact_header');?>
                        </div>
                    </div>
                </div>
                  <div class="row">
                    <div class="col-md-8 col-md-offset-2 col-sm-12 col-xs-12">
                        <div class="contact-form-area" data-aos="fade-left" data-aos-once="true">
                            <?php if (get_locale() == 'en_US') {
                                echo do_shortcode('[contact-form-7 id="409" title="Contact Form English"]');
                            }else{
                                echo do_shortcode('[contact-form-7 id="408" title="نموذج الاتصال بنا"]');
                            }?>
                        </div>	
                    </div>
                </div>
            </div>
        </div>
    </section>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD5XgHmjdDawVA3rqdOcE4N59LYz1RCSE0&callback=initMap" async defer></script>
<?php get_footer(); ?>
