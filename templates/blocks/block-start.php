<?php
$block_header = get_sub_field('header');
$block_desc = get_sub_field('description');
$block_items = get_sub_field('items');
?>
<section class="start text-center">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h3 data-aos="fade-down" data-aos-once="true"><?php echo $block_header;?></h3>
                <p data-aos="fade-down" data-aos-delay="30" data-aos-once="true"><?php echo $block_desc;?></p>
            </div>
        </div>
        <div class="container start-content">
        <div class="row" data-aos="fade-down" data-aos-delay="300" data-aos-once="true">
          <?php $step = 0;if(is_array($block_items)): foreach($block_items as $item):$step++;
          $item_icon = $item['icon'];
          if (empty($item_icon))
            $item_icon = get_template_directory_uri()."/assets/img/Vectoe1.png";
          ?>
            <div class="col-md-4">
                <img class="img-responsive img-start"src="<?php echo $item_icon;?>"/>
                <div class="step<?php echo($step == 3)?' last-left':'';?>"><?php echo $step;?></div>
                <h4><?php echo $item['title'];?></h4>
                <p><?php echo $item['description'];?></p>
            </div>
          <?php endforeach;endif;?>
        </div>
    </div>
    </div>
</section>
