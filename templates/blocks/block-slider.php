<?php
$block_slider_type = get_sub_field('type');
$block_items = get_sub_field('items');
$frontpage_id = get_option( 'page_on_front' );
$slider_custom_shortcode = get_sub_field( 'slider_custom_shortcode' );
?>
<?php if($block_slider_type == 'custom_slider'):?>
    <?php echo do_shortcode($slider_custom_shortcode);?>
<?php elseif($block_slider_type == 'carousel_slider'):?>

    <section class="work_slider_container">
    <div class="work_slider">
    <?php if(is_array($block_items)):$count = 0; foreach($block_items as $item):?>
        <div class="item">
            <div class="col-md-6 col-sm-6 col-xs-6">
                <div class="slide-img">
                    <img class="img-responsive" data-animation="fadeInRight" data-delay="0.5s" src="<?php echo $item['image'];?>"/>
                </div>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-6">
                <div class="slide-text">
                    <img data-animation="<?php if (get_locale() == 'en_US') { echo"fadeInRight";}else{echo"fadeInLeft";}?>" data-delay="0.5s" src="<?php echo $item['icon'];?>"/>
                    <h5 data-animation="fadeInDown" data-delay="1s" ><?php echo $item['title'];?></h5>
                    <ul data-animation="fadeInDown" data-delay="1s" >
                        <?php 
                            $bits = explode("\n", $item['desc']);
                            foreach($bits as $bit){
                                echo "<li>" . $bit . "</li>";
                            }
                        ?> 
                    </ul>
                </div>
            </div>
        </div>
    <?php $count++;endforeach;endif;?>
    </div>
    </section>
    
<?php elseif($block_slider_type == 'home_slider_custom'):?>

        <section class="main_slider slider">
            <?php if(is_array($block_items)):$count = 0; foreach($block_items as $item):?>
                <div class="item">
                    <div class="bg_imgslider" style="background-image:url('<?php echo $item['image'];?>')"></div>
                    <div class="slider-container">
                        <h2 class="slide2Txt1" data-animation="fadeInLeft" data-delay="0.5s"><?php echo $item['title'];?> </h2>
                        <p class="slide2Txt2 text-color" data-animation="fadeInLeft" data-delay="0.5s">
                            <?php echo $item['desc'];?>
                        </p>
                        <br /><br />
                    <?php if(!empty($item['button_one_url'])):?>
                        <a href="<?php echo $item['button_one_url'];?>" class="btn btn-primary btn-lg active ticket" role="button" data-animation="fadeInLeft" data-delay="0.5s"> <?php echo $item['button_one_text'];?></a>
                    <?php endif;?>
                    <?php if(!empty($item['button_two_url'])):?>
                        <a href="<?php echo $item['button_two_url'];?>" class="btn btn-default btn-lg active account" role="button" data-animation="fadeInLeft" data-delay="0.5s"> <?php echo $item['button_two_text'];?></a>                  
                    <?php endif;?>        
                    </div>
                </div>
            <?php $count++;endforeach;endif;?>

        </section>

<?php elseif($block_slider_type == 'carousel_slider2'):?>

<section class="why">
<div class="container">
    <div class="why-slider">

    <?php if(is_array($block_items)):$count = 0; foreach($block_items as $item):?>
        <div class="why-content">
            <div class="row">
                <div class="col-md-12">
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <div class="why-img">
                            <img class="img-responsive" data-animation="<?php if (get_locale() == 'en_US') { echo"fadeInLeft";}else{echo"fadeInRight";}?>" data-delay="0.5s" src="<?php echo $item['image'];?>"/>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <div class="why-text" data-animation="fadeInDown" data-delay="0.5s">
                            <h4><?php echo $item['title'];?></h4>
                            <p><?php echo $item['desc'];?></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?php $count++;endforeach;endif;?>
    </div>
</div>
</section>
<?php else:?>
    <?php if( have_rows('header_slider' , $frontpage_id) ):?>
        <section class="main_slider slider">
            <?php while ( have_rows('header_slider' , $frontpage_id) ) : the_row();?>

                <div class="item">
                    <div class="bg_imgslider" style="background-image:url('<?php the_sub_field('background_image');?>')"></div>
                    <div class="slider-container">
                        <h2 class="slide2Txt1" data-animation="fadeInLeft" data-delay="0.5s"><?php the_sub_field('title');?> </h2>
                        <p class="slide2Txt2 text-color" data-animation="fadeInLeft" data-delay="0.5s">
                            <?php the_sub_field('description');?>
                        </p>
                        <br /><br />
                        <?php if(!empty(get_sub_field('button_one_url'))):?>
                            <a href="<?php the_sub_field('button_one_url');?>" class="btn btn-primary btn-lg active ticket" role="button" data-animation="fadeInLeft" data-delay="0.5s"> <?php the_sub_field('button_one_text');?></a>
                        <?php endif;?>
                        <?php if(!empty(get_sub_field('button_two_url'))):?>
                            <a href="<?php the_sub_field('button_two_url');?>" class="btn btn-default btn-lg active account" role="button" data-animation="fadeInLeft" data-delay="0.5s"><?php the_sub_field('button_two_text');?></a>                  
                    <?php endif;?>
                    </div>
            </div>
            <?php endwhile;?>
        </section>
    <?php endif;?>

<?php endif;?>
