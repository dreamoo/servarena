<?php
$block_title = get_sub_field('title');
$block_desc = get_sub_field('description');
$block_items = get_sub_field('items');
?>
<section class="domains_details text-center">
  <div class="container">
      <div class="row">
          <div class="col-md-12 ">
              <div class="domain-title" data-aos="fade-down" data-aos-once="true">
                  <div class="title-group">
                      <h2><?php echo $block_title;?></h2>
                      <p><?php echo $block_desc;?></p>
                  </div>
              </div>
          </div>
      </div>
      <div class="row">
        <?php if(is_array($block_items)): foreach($block_items as $item):?>
          <div class="col-md-3 col-sm-6 col-xs-12">
              <div class="tld" data-aos="fade-down" data-aos-once="true" data-aos-delay="200">
                  <h3 class="tld-title"><span><?php echo $item['name'];?></span></h3>
                  <ul>
                     <?php 
                      $bits = explode("\n", $item['price']);
                       foreach($bits as $bit){
                          echo "<li>" . $bit . "</li>";
                        }
                     ?> 
                  </ul>
              </div>
          </div>
        <?php endforeach;endif;?>
      </div>
  </div>
</section>