<section class="host_service text-center">
    <div class="hostService-content">
        <div class="nav-panel">
            <div class="nav-line">
              <ul class="list-inline">
                <?php $links = get_sub_field('links');
                  if(is_array($links)): foreach ($links as $id): ?>
                  <li class="nav-item <?php echo (get_the_ID() == $id )?' active':'';?>">
                    <a class="nav-link" href="<?php echo esc_url( get_permalink($id) );?>" >
                        <?php echo get_the_title( $id );?>
                    </a>
                  </li>
                <?php endforeach;endif;?>
              </ul>
          </div>
        </div>
    </div>
</section>
