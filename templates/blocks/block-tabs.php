<?php
$block_title = get_sub_field('title');
$block_desc = get_sub_field('desc');
?>

<section class="condition text-center">
    <div class="container">
        <?php if($block_title != ''):?>
        <div class="row">
            <div class="col-xs-12">
                <h3 class="section-title"><?php echo $block_title;?></h3>
                <p class="section-desc"><?php echo $block_desc;?> </p>
            </div>
        </div>
        <?php endif;?>
        <div class="row">
            <div class="col-xs-11 col-xs-offset-1 ">
                <div class="terms">
                <?php if( have_rows('items') ):?>
                    <ul class="nav nav-tabs " role="tablist">
                    <?php $count=0; while ( have_rows('items') ) : the_row();$count++;?>
                        <li <?php echo($count == 1)? ' class="active"':'';?>>
                            <a class="tablink-prop" href="#tab-<?php echo $count;?>" role="tab" data-toggle="tab">
                                <?php the_sub_field('title');?>
                            </a>
                        </li>
                    <?php endwhile;?>
                    </ul>
                <?php endif;?>
                </div>
            </div>
        </div>
    </div>
<?php if( have_rows('items') ):?>
    <div class="row tab-content-bg">
        <div class="col-xs-offset-2 col-xs-9 ">
                <!-- Tab panes -->
            <div class="tab-content tab-contentProp">
              <?php $count=0; while ( have_rows('items') ) : the_row();$count++;?>
                <div role="tabpanel" class="tab-pane<?php echo($count == 1)? ' active':'';?>" id="tab-<?php echo $count;?>">
                    <?php the_sub_field('data');?>
                </div>
              <?php endwhile;?>
            </div>
        </div>
    </div>
<?php endif;?>
</section>