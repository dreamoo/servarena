<?php
$block_header = get_sub_field('header');
$block_items = get_sub_field('items');
$sec_arr = ['first_sector','sec_sector','third_sector','fourth_sector','fifth_sector','six_sector'];
$sec_icon_arr = ['cart','grad','health','gover','company','ngo'];
?>
<section class="sectors">
    <div class="container">
        <div class="row">
                <div class="col-md-offset-1 col-md-5">
                    <div class="main-title">
                        <div class="sp-title title-group" data-aos="fade-down" data-aos-once="true">
                            <?php echo $block_header;?> 
                        </div>
                    </div>
                </div>
                <div class="col-md-6 img_section">
                <?php if(is_array($block_items)):$count = 0; foreach($block_items as $item):?>
                    <div class="col-md-4 col-xs-6 sector_sec <?php echo $sec_arr[$count];?>">
                        <div class="img-sector" data-aos="fade-left" data-aos-once="true"><div class="img-sector-prop <?php echo $sec_icon_arr[$count];?>"></div></div>
                        <h6 data-aos="fade-left" data-aos-once="true"><?php echo $item['title'];?></h6>
                    </div>
                <?php $count++;endforeach;endif;?>
                </div>
        </div>
    </div>
</section>
