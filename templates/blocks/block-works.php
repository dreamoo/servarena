<?php
$block_header = get_sub_field('header');
$block_items = get_sub_field('items');
$read_more_btn = get_sub_field('read_more_btn');
?>
<section class="clients">
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="ourclient-title title-after text-center">
                <div class="title-group" data-aos="fade-down" data-aos-once="true">
                    <h3><?php echo $block_header;?></h3>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
        <?php $count = 0;if(is_array($block_items)): foreach($block_items as $item):$count++;
          $item_icon = $item['icon'];
          if (empty($item_icon))
            $item_icon = get_template_directory_uri()."/assets/img/Vectoe1.png";
          ?>
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="img-sec" data-toggle="modal" data-target="#Modal<?php echo $count;?>">
                    <img data-aos="fade-down" data-aos-once="true" class="img-responsive" src="<?php echo $item_icon;?>"/>
                </div>
            </div>

            <div id="Modal<?php echo $count;?>" class="modal fade" tabindex="-1" role="dialog">
                <div class="modal-dialog text-center">
                    <div class="modal-content">
                        <div class="modal-body">
                            <img data-aos="fade-down" data-aos-once="true" src="<?php echo $item_icon;?>" class="img-responsive">
                            <p data-aos="fade-down" data-aos-once="true"><?php echo $item['description'];?></p>
                            <span data-aos="fade-down" data-aos-once="true">- <?php echo $item['title'];?> -</span>
                        </div>
                    </div>
                </div>
            </div>
          <?php endforeach;endif;?>
        </div>
    </div>
    <?php if(!empty($read_more_btn)):?>
    <div class="row">
        <div class="col-md-12 text-center" data-aos="fade-down" data-aos-once="true">
            <a href="<?php echo $read_more_btn;?>" class="btn btn-default btn-lg active client" role="button"><?php _e('more','mdwp');?></a>
        </div>
    </div>
    <?php endif;?>
</div>
</section>