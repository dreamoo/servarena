<?php
$block_header = get_sub_field('header');
$block_items = get_sub_field('items');
?>

<section class="faq">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="main-title">
                    <div class="sp-title title-group">
                        <?php echo $block_header;?> 
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="panel-group" id="accordion">
                <?php if(is_array($block_items)):$count = 0; foreach($block_items as $item):?>
                    <div class="panel panel-default">
                        <div class="panel-heading accordion-toggle question-toggle collapsed" data-toggle="collapse" data-parent="#accordion" data-target="#collapse<?php echo $count;?>">
                            <h4 class="panel-title">
                                <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo $count;?>">
                                    <?php echo $item['title'];?>
                                </a>
                            </h4>
                        </div>
                        <div id="collapse<?php echo $count;?>" class="panel-collapse collapse">
                            <div class="panel-body">
                                <?php echo $item['data'];?>
                            </div>
                        </div>
                    </div>
                <?php $count++;endforeach;endif;?>
                </div>
            </div>
        </div>
    </div>
</section>