<?php
$block_header = get_sub_field('header');
$block_data = get_sub_field('data');
?>

<section class="content">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
            <?php if($block_header != ''):?>
                <div class="main-title" data-aos="fade-down" data-aos-once="true" data-aos-delay="400">
                    <div class="sp-title title-group">
                        <?php echo $block_header;?> 
                    </div>
                </div>
            <?php endif;?>
            </div>
            <div class="col-md-12" data-aos="fade-down" data-aos-once="true" data-aos-delay="400">
                <?php echo $block_data;?> 
            </div>
        </div>
    </div>
</section>