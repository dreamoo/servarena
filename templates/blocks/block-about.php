<?php
$block_header = get_sub_field('header');
$block_desc = get_sub_field('description');
$block_link = get_sub_field('more_link');
$block_side_image = get_sub_field('side_image');
if(empty($block_side_image))
  $block_side_image = get_template_directory_uri().'/assets/img/about1.png';
?>
<section class="about text-right">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="main-title" data-aos="fade-down" data-aos-once="true">
                    <div class="title-group">
                        <?php echo $block_header;?>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
        <div class="col-md-offset-1 col-md-5 col-sm-12 col-xs-12">
            <div class="about-content" data-aos="fade-down"  data-aos-delay="300" data-aos-once="true">
                <?php echo $block_desc; ?>
                <?php if(!empty($block_link)):?>
                  <a href="<?php echo $block_link;?>" class="btn btn-default btn-lg active order" role="button"><?php if (get_locale() == 'en_US') { echo "Order Now";}else{echo "اطلب الان";}?></a>
                <?php endif;?>
                
            </div>
        </div>
        <div class="col-md-6 col-sm-6 col-hidden-xs">
            <div data-aos="fade-right" data-aos-once="true" class="server_img" style="background-image: url(<?php echo $block_side_image;?>)"></div>
            </div>
        </div>
     </div>
</section>
