<?php
$block_header = get_sub_field('header');
$block_items = get_sub_field('items');
$block_side_image = get_sub_field('side_image');
$block_large_img = get_sub_field('large_img');
$block_small_view = get_sub_field('small_view');
if(empty($block_side_image))
  $block_side_image = get_template_directory_uri().'/assets/img/vectorFeat.png';
?>
<?php if($block_small_view == "small_1"):?>
<section class="advantages-small"> 
    <div class="page-content">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="main-title" data-aos="fade-down" data-aos-once="true">
                        <div class="title-group">
                            <?php echo $block_header;?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row adv-data" data-aos="fade-down" data-aos-delay="300" data-aos-once="true">
                <?php if(is_array($block_items)): foreach($block_items as $item):
                $item_icon = $item['icon'];
                if(empty($item_icon))
                    $item_icon = get_template_directory_uri().'/assets/img/cart.png';
                ?>
                <div class="col-xs-6 col-md-3">
                    <div class="adv-item">
                        <img data-aos="fade-down" data-aos-delay="400" data-aos-once="true" src="<?php echo $item_icon;?>" alt="<?php echo $item['title'];?>" />
                        <h4 data-aos="fade-down" data-aos-delay="500" data-aos-once="true"><?php echo $item['title'];?> </h4>
                        <p data-aos="fade-down" data-aos-delay="550" data-aos-once="true"><?php echo $item['description'];?></p>
                        <a data-aos="fade-down" data-aos-delay="550" data-aos-once="true" href=" <?php echo $item['link'];?>" class="btn read-more"><?php _e('order now','mdwp');?></a>
                    </div>
                </div>
                <?php endforeach;endif;?>
            </div>
        </div>
    </div>
</section>
<?php elseif($block_small_view == "small_2"):?>
<section class="shared_hosting"> 
    <div class="page-content">
        <section class="server_feature">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div data-aos="fade-down" data-aos-once="true">
                            <?php echo $block_header;?>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="servers-icon" style="width:100%">
                      <?php $i=0;$n=0;if(is_array($block_items)): foreach($block_items as $item):$i++;$n++;
                        $item_icon = $item['icon'];
                        if(empty($item_icon))
                            $item_icon = get_template_directory_uri().'/assets/img/web1.png';
                        ?>
                        <?php echo($i == 1)?'<div class="slide_container">':'';?>
                        <div class="col-md-4 col-sm-6 col-xs-12 adv-item">
                            <div class="server_card">
                                <span data-animation="<?php if (get_locale() == 'en_US') { echo"fadeInRight";}else{echo"fadeInLeft";}?>" data-delay="0.5s" class="web-icon" style="background-image:url(<?php echo $item_icon;?>)"></span>
                                <h4 data-aos="fade-left" data-aos-once="true" data-aos-delay="200" data-animation="<?php if (get_locale() == 'en_US') { echo"fadeInRight";}else{echo"fadeInLeft";}?>" data-delay="1s" ><?php echo $item['title'];?> </h4>
                                <p data-aos="fade-left" data-aos-once="true" data-aos-delay="200" data-animation="<?php if (get_locale() == 'en_US') { echo"fadeInRight";}else{echo"fadeInLeft";}?>" data-delay="1s" ><?php echo $item['description'];?></p>
                            </div>
                        </div>
                        <?php if($n == 6 && $i != count($block_items)){
                            echo '</div><div class="slide_container">';
                            $n = 0;
                        }?>
                        <?php echo($i == count($block_items))?'</div>':''?>
                      <?php endforeach;endif;?>
                   </div>
                </div>
            </div>
        </section>
    </div>
</section>

<?php else:?>
<section class="shared_hosting"> 
    <div class="page-content">
        <section class="server_feature">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="main-title" data-aos="fade-down" data-aos-once="true">
                            <div class="title-group">
                                <?php echo $block_header;?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                
                    <div data-aos="fade-up" data-aos-delay="1000" data-aos-duration="5000" data-aos-once="true" class="server_img<?php echo ($block_large_img)?' large_img':'';?>" style="background-image: url(<?php echo $block_side_image;?>)"></div>
                    <div class="servers-icon">
                      <?php $i=0;$n=0;if(is_array($block_items)): foreach($block_items as $item):$i++;$n++;
                        $item_icon = $item['icon'];
                        if(empty($item_icon))
                            $item_icon = get_template_directory_uri().'/assets/img/web1.png';
                        ?>
                        <?php echo($i == 1)?'<div class="slide_container">':'';?>
                        <div class="col-md-6 col-sm-6 col-xs-12 adv-item">
                            <div class="server_card">
                                <span data-animation="<?php if (get_locale() == 'en_US') { echo"fadeInRight";}else{echo"fadeInLeft";}?>" data-delay="0.5s" class="web-icon" style="background-image:url(<?php echo $item_icon;?>)"></span>
                                <h4 data-aos="fade-left" data-aos-once="true" data-aos-delay="200" data-animation="<?php if (get_locale() == 'en_US') { echo"fadeInRight";}else{echo"fadeInLeft";}?>" data-delay="1s" ><?php echo $item['title'];?> </h4>
                                <p data-aos="fade-left" data-aos-once="true" data-aos-delay="200" data-animation="<?php if (get_locale() == 'en_US') { echo"fadeInRight";}else{echo"fadeInLeft";}?>" data-delay="1s" ><?php echo $item['description'];?></p>
                            </div>
                        </div>
                        <?php if($n == 6 && $i != count($block_items)){
                            echo '</div><div class="slide_container">';
                            $n = 0;
                        }?>
                        <?php echo($i == count($block_items))?'</div>':''?>
                      <?php endforeach;endif;?>
                   </div>
                </div>
            </div>
        </section>
    </div>
</section>
<?php endif;?>