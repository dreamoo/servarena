<?php
$block_header = get_sub_field('header');
$block_desc = get_sub_field('description');
$block_items = get_sub_field('items');
?>
<section class="payment_bank">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="main-title">
                    <div class="title-group">
                        <h3><?php echo $block_header;?></h3>
                        <p><?php echo $block_desc;?></p>
                    </div>
                </div>
            </div>
        </div> 
        <br /><br /><br />
        <div class="row">
            <div class="col-md-10 col-md-offset-1 col-sm-11 col-xs-11">
                <div class="payment_table table-responsive">
                <table id="payment" class="table table-striped">
                    <thead>
                        <tr>
                            <th class="text-center"></th>
                            <th class="text-center"><?php _e('Bank Name','mdwp');?></th>
                            <th class="text-center"><?php _e('Account Name','mdwp');?></th>
                            <th class="text-center"> <?php _e('Account Number','mdwp');?></th>
                            <th class="text-center"> <?php _e('IBAN','mdwp');?></th>
                            <th class="text-center"><?php _e('Swift Code','mdwp');?></th>
                            <th class="text-center"></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php if(is_array($block_items)): foreach($block_items as $item):
                                $item_icon = $item['icon'];
                                if (empty($item_icon))
                                    $item_icon = get_template_directory_uri()."/assets/img/ahly_bank.png";
                        ?>
                            <tr>
                                <td><img src="<?php echo $item_icon;?>" /></td>
                                <td class="bank_name"><?php echo $item['bank_name'];?></td>
                                <td><?php echo $item['account_name'];?></td>
                                <td>  <?php echo $item['account_number'];?> </td>
                                <td><?php echo $item['account_iban'];?></td>
                                <td><?php echo $item['account_swift'];?></td>
                                <td>
                                <?php if(!empty($item['account_currency'])):?>
                                <button type="button" class="btn btn-success bank_price"><?php echo $item['account_currency'];?></button>
                                <?php endif;?>
                                </td>
                            </tr>
                        <?php endforeach;endif;?>
                    </tbody>
                </table>
                </div>
            </div>
        </div>
    </div>
</section>