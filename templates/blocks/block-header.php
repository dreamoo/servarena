<?php 
$block_title = get_sub_field('title');
$block_desc = get_sub_field('description');
$block_img = get_sub_field('image');
$block_bgcolor = get_sub_field('bg_color');
$order_now_link = get_sub_field('order_now_link');
$custom_bg = get_sub_field('custom_bg');
if(empty($block_img))
  $block_img = get_template_directory_uri().'/assets/img/insideslider.png';
?>
<div class="slider_insidepage">
    <section class="<?php echo($block_bgcolor == 'green')?'prog_slide':'green_slider';?> slider">
        <div class="inside-content container">
            <div class="row">
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <div class="slide-text">
                            <h2><?php echo $block_title;?></h2>
                            <p><?php echo $block_desc;?></p>
                            <?php if(!empty($order_now_link)):?>
                                <a class="btn btn-price price-prop" href="<?php echo $order_now_link;?>"><?php _e('order now','mdwp');?></a>
                            <?php endif;?>
                        </div>
                    </div>
                    <div class="col-md-5 col-sm-5 col-xs-12">
                        <div class="slide-img">
                            <img class="img-responsive" src="<?php echo $block_img;?>"/>
                        </div>
                    </div>
            </div>
        </div>
    </section>
</div>