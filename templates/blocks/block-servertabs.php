<?php
$block_titles = get_sub_field('titles');
$block_tabs = get_sub_field('tabs');
$block_items = get_sub_field('items');
?>
<section class="table_info">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="tabbable-panel">
                <ul class="nav nav-pills nav-fill" data-aos="fade-down" data-aos-once="true">
                <?php if(is_array($block_tabs)):$t = 0; foreach($block_tabs as $tab):$t++;?>
                    <li class="nav-item<?php echo ($t == 1)? ' active':'';?>">
                        <a class="nav-link" href="#tab_<?php echo $t;?>" data-toggle="tab"><i class="<?php echo $tab['icon'];?> table_icon"></i> <span><?php echo $tab['title'];?></span></a>
                    </li>
                <?php endforeach;endif;?>
                </ul>
                <div class="tab-content">
                <?php if(is_array($block_tabs)):$count = 0; foreach($block_tabs as $tab):$count++;?>
                    <div data-aos="fade-down" data-aos-once="true" data-aos-delay="200" class="tab-pane<?php echo ($count == 1)? ' active':'';?>" id="tab_<?php echo $count;?>">
                        <div class="data_table table-responsive">
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th><?php echo $block_titles['title1'];?></th>
                                        <th><?php echo $block_titles['title2'];?></th>
                                        <th><?php echo $block_titles['title3'];?></th>
                                        <th><?php echo $block_titles['title4'];?></th>
                                        <th><?php echo $block_titles['title5'];?></th>
                                    </tr>
                                </thead>
                                <tbody>
                            <?php if(is_array($tab['items'])):foreach($tab['items'] as $item):?>
                                <tr>
                                    <td class="feature"><?php echo $item['title1'];?></td>
                                    <td>
                                        <?php 
                                            if($item['title2'] == "yes"):
                                                echo '<i class="fas fa-check"></i>';
                                            elseif($item['title2'] == "no"):
                                                echo '<i class="fas fa-times"></i>';
                                            else:
                                                echo $item['title2'];
                                            endif;
                                        ?>
                                    </td>
                                    <td>
                                        <?php 
                                            if($item['title3'] == "yes"):
                                                echo '<i class="fas fa-check"></i>';
                                            elseif($item['title3'] == "no"):
                                                echo '<i class="fas fa-times"></i>';
                                            else:
                                                echo $item['title3'];
                                            endif;
                                        ?>
                                    </td>
                                    <td>
                                        <?php 
                                            if($item['title4'] == "yes"):
                                                echo '<i class="fas fa-check"></i>';
                                            elseif($item['title4'] == "no"):
                                                echo '<i class="fas fa-times"></i>';
                                            else:
                                                echo $item['title4'];
                                            endif;
                                        ?>
                                    </td>
                                    <td>
                                        <?php 
                                            if($item['title5'] == "yes"):
                                                echo '<i class="fas fa-check"></i>';
                                            elseif($item['title5'] == "no"):
                                                echo '<i class="fas fa-times"></i>';
                                            else:
                                                echo $item['title5'];
                                            endif;
                                        ?>
                                    </td>
                                </tr>
                            <?php endforeach;endif;?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                <?php endforeach;endif;?>
                </div>
            </div>
        </div>
    </div>
</div>
</section>