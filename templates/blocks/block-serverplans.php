<?php
$block_title = get_sub_field('block_title');
$block_desc = get_sub_field('block_desc');
$block_plans = get_sub_field('plans');
$block_col4 = get_sub_field('col4');
$block_col2 = get_sub_field('col2');
$block_orangebg = get_sub_field('orangebg');
?>
<?php if($block_col2):?>

<section class="managment_plan">
<div class="container">
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="manage-title text-center" data-aos="fade-down" data-aos-once="true">
                <h3><?php echo $block_title;?> </h3>
                <p><?php echo $block_desc;?></p>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 col-xs-12">
        <?php if(is_array($block_plans)): foreach($block_plans as $item):?>
            <div class="col-md-6 col-sm-6 col-xs-12" data-aos="fade-down" data-aos-once="true" data-aos-delay="300">
                <div class="single-price frist-price card text-left">
                    <div class="price-header">
                        <h5 class="fontweight-bold"> <?php echo $item['title'];?> </h5>
                    </div>
                    <ul class="ul-priceprop">
                        <?php 
                            $bits = explode("\n", $item['info']);
                            foreach($bits as $bit){
                                echo '<li class="li-priceprop">' . $bit . '</li>';
                            }
                        ?> 
                    </ul>
                    <div>
                        <?php if(!empty($item['link'])):?>
                        <a class="btn btn-price price-prop" href="<?php echo $item['link'];?>"><?php _e('order now','mdwp');?>  </a>
                        <?php endif;?>
                        <div class="plan_price">
                            <h2><span class="currency"></span><?php echo $item['price'];?></h2>
                        </div>
                    </div>
                </div>
            </div>
        <?php endforeach;endif;?>
        </div>
    </div>
    </div>
</section>

<?php else:?>

<section class="server_plans<?php echo($block_orangebg)?' orangebg':'';?> text-center">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="serverPlan-title" data-aos="fade-down" data-aos-once="true">
                    <div class="title-group">
                        <h3><?php echo $block_title;?> </h3>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
           <div class="container">
                <div data-aos="fade-down" data-aos-once="true" data-aos-delay="300" class="<?php echo($block_col4)?'col-md-12':'col-md-10 col-md-offset-1 col-xs-12';?>">
                  <?php if(is_array($block_plans)): foreach($block_plans as $item):?>
                    <div class="col-md-<?php echo($block_col4)?'3':'4';?> col-sm-6 col-xs-12">
                        <div class="single-price frist-price card wow flipInY <?php echo($item['active'])?' active':''; echo($item['blur_item'])?' div_blur':'';?>" <?php echo($item['active'])?'data-active="true"':'';?> data-wow-offset="10" data-wow-delay="0.2s">
                           <div class="price-header">
                               <h5> <?php echo $item['title'];?>  </h5>
                           </div>
                           <div class="price">
                               <h2><span class="currency"></span><?php echo $item['price'];?></h2>
                               <span class="per">/ <?php _e('Month','mdwp');?> </span>
                           </div>
                          <ul >
                             <?php 
                              //echo '<li>' . str_replace("\n", '</li><li>', $item['info']) . '</li>';
                              $bits = explode("\n", $item['info']);
                               foreach($bits as $bit){
                                  echo "<li>" . $bit . "</li>";
                                }
                             ?> 
                           </ul>
                           <?php if(!empty($item['link'])):?>
                            <a class="btn btn-price" href="<?php echo $item['link'];?>"><?php _e('order now','mdwp');?>  </a>
                           <?php endif;?>
                       </div>
                     </div>
                  <?php endforeach;endif;?>
                </div>
             </div>
        </div>
    </div>
</section>
<?php endif;?>