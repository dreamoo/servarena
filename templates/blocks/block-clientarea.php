<?php
$block_title = get_sub_field('title');
$block_btn_title = get_sub_field('btn_title');
$block_btn_link = get_sub_field('btn_link');
$block_bgcolor = get_sub_field('bg_color');
if($block_bgcolor == 'blue'): 
        $containerClass = 'order_area';
        $titleClass = 'order-title';
        $btnClass = 'btn btn-default btn-lg active order';
    elseif($block_bgcolor == 'yellow'): 
        $containerClass = 'order_mobileapp';
        $titleClass = 'mobileapp-title';
        $btnClass = 'btn btn-dangar btn-lg active mobile_app_btn';
    else: 
        $containerClass = 'client_area';
        $titleClass = 'client-title';
        $btnClass = 'btn btn-default btn-lg active client';
    endif;
?>
<section class="<?php echo $containerClass;?> text-center">
    <div class="overlay"></div>
    <div class="container">
        <div class="row" data-aos="fade-down" data-aos-once="true" data-aos-delay="400">
            <div class="col-md-12">
                <div class="<?php echo $titleClass;?>">
                    <div class="title-group">
                       <p><?php echo $block_title;?></p>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <a href="<?php echo $block_btn_link;?>" class="<?php echo $btnClass;?>" role="button"><?php echo $block_btn_title;?> </a>
            </div>
        </div>
    </div>
</section>