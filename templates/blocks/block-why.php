<?php
$block_header = get_sub_field('header');
$block_subheader = get_sub_field('sub-header');
$block_items = get_sub_field('items');
$block_view = get_sub_field('view');
$block_view_col4 = get_sub_field('col4');
$block_view_large_title = get_sub_field('view_title');
?>
<?php if($block_view == 'view_large'):?>

<section class="why_order text-center">
    <div class="container">
        <div class="row" data-aos="fade-down" data-aos-once="true" data-aos-delay="400">
            <div class="col-md-12">
                <h3><?php echo $block_header;?></h3>
            </div>
            <div class="col-md-offset-2 col-md-8">
                <p><?php echo $block_subheader;?></p>
            </div>
        </div>
        <div class="order-content">
                <div class="row">
                <?php if(is_array($block_items)): foreach($block_items as $item):
                    $item_icon = $item['icon'];
                    if (empty($item_icon))
                        $item_icon = get_template_directory_uri()."/assets/img/vect.png";
                ?>
                <div data-aos="fade-down" data-aos-delay="300" data-aos-once="true" class="col-md-<?php echo($block_view_col4)?'3':'4';?> order-sec">
                    <img class="img-responsive img-order" src="<?php echo $item_icon ;?>">
                    
                    <?php 
                    if($block_view_large_title): 
                        $description = explode("\n", $item['description'], 2);
                        echo '<h4><b>'.$description[0].'</b></h4>';
                        echo '<p>'.$description[1].'</p>';
                    else: 
                        echo '<h4>'.$item['description'].'</h4>';
                    endif;
                    ?>

                </div>
                <?php endforeach;endif;?>

            </div>
        </div>
    </div>
</section>
<?php else:?>
<section class="why_servarena text-right">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="serverFeat-title" data-aos="fade-down" data-aos-once="true">
                    <div class="title-group">
                        <h2><?php echo $block_header;?></h2>
                        <h3><?php echo $block_subheader;?></h3>
                    </div>
                </div>
            </div>
        </div>
        <div class="row" data-aos="fade-down" data-aos-once="true" data-aos-delay="400">
          <?php if(is_array($block_items)): foreach($block_items as $item):
          $item_icon = $item['icon'];
          if (empty($item_icon))
            $item_icon = get_template_directory_uri()."/assets/img/vect.png";
          ?>
            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="why-content">
                    <img src="<?php echo $item_icon ;?>"/>
                    <span><?php echo $item['description'];?></span>
                </div>
            </div>
          <?php endforeach;endif;?>
        </div>
    </div>
</section>

<?php endif;?>
