<?php
$block_content = get_sub_field('content');
$block_btn_title = get_sub_field('btn_title');
$block_btn_link = get_sub_field('btn_link');
?>
<section class="contact_web">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                    <div class="col-md-6 col-sm-6 col-xs-12">
                    <div class="slide-img" data-aos="fade-right" data-aos-once="true">
                        <img class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/assets/img/contactweb.png"/>
                    </div>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <div class="slide-text" data-aos="fade-down" data-aos-once="true" data-aos-delay="200">
                        <?php echo $block_content;?>
                        <?php if(!empty($block_btn_link)):?>
                        <a class="btn btn-contact" href="<?php echo $block_btn_link;?>"><?php echo $block_btn_title;?> </a>
                        <?php endif;?>
                    </div>
                </div>
            </div> 
        </div>
    </div>
</section>