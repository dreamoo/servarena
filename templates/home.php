<?php /* Template Name: Homepage */ 
//echo get_locale();
get_header(); ?>
     <!-- ======================Slider============================ -->
<?php if( have_rows('header_slider') ):?>

    <?php if( get_field('active_custom_slider') ):?>
        <?php echo do_shortcode(get_field('custom_slider_shortcode'));?>
    <?php else:?>
      <section class="main_slider slider">
        <?php while ( have_rows('header_slider') ) : the_row();?>

            <div class="item">
                 <div class="bg_imgslider" style="background-image:url('<?php the_sub_field('background_image');?>')"></div>
                <div class="slider-container">
                    <h2 class="slide2Txt1" data-animation="fadeInLeft" data-delay="0.5s"><?php the_sub_field('title');?> </h2>
                    <p class="slide2Txt2 text-color" data-animation="fadeInLeft" data-delay="0.5s">
                        <?php the_sub_field('description');?>
                    </p>
                    <br /><br />
                    <?php if(!empty(get_sub_field('button_one_url'))):?>
                        <a href="<?php the_sub_field('button_one_url');?>" class="btn btn-primary btn-lg active ticket" role="button" data-animation="fadeInLeft" data-delay="0.5s"> <?php the_sub_field('button_one_text');?></a>
                    <?php endif;?>
                    <?php if(!empty(get_sub_field('button_two_url'))):?>
                        <a href="<?php the_sub_field('button_two_url');?>" class="btn btn-default btn-lg active account" role="button" data-animation="fadeInLeft" data-delay="0.5s"><?php the_sub_field('button_two_text');?></a>                  
                   <?php endif;?>
                </div>
           </div>
        <?php endwhile;?>
      </section>
    <?php endif;?>
<?php endif;?>

     <!-- ======================features============================ -->

      <?php if( have_rows('features_block') && get_field('active_features_block') ):?>
         <section class="features">
             <div class="container">
                <div class="row">
                <?php while ( have_rows('features_block') ) : the_row();?>

                    <div class="col-md-3 col-sm-6 col-xs-6 feat-content">
                        <div class="feature">
                            <div class="img-continer">
                                <a href="#"><img data-aos="fade-down" data-aos-once="true" src="<?php the_sub_field('icon');?>" data-toggle="tooltip" data-placement="top" title="استضافة مواقع" style="display:inline-block"/></a>
                                <span class="line"></span>
                                <figcaption>
                                    <h4 data-aos="fade-down" data-aos-delay="100" data-aos-once="true"><?php the_sub_field('title');?></h4>
                                    <p data-aos="fade-down" data-aos-delay="150" data-aos-once="true"><?php the_sub_field('description');?></p>
                                </figcaption>
                            </div>
                        </div>
                    </div>

                <?php endwhile;?>

                </div>
             </div>
         </section>

      <?php endif;?>

      <!-- ======================domains============================ -->
<?php if( get_field('active_domains_block') ):?>
         <section class="domains">
             <div class="container">
                <div class="domain">
                <div class="row">
                    <div class="col-md-5">
                        <div class="main-title"  data-aos="fade-down" data-aos-once="true">
                            <div class="title-group">
                                <?php echo the_field('domains_block_head');?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-7">
                        <div class="domain-tld">
                            <div class="row">
                                <div class="col-md-9 col-sm-12 col-xs-12">
                                    <div class="domain-search"  data-aos="fade-down" data-aos-once="true">
                                       <form class="navbar-form" role="search">
                                            <div class="input-group add-on">
                                              <input type="text" class="form-control" placeholder="<?php if (get_locale() == 'en_US') { echo "Search for a domain";}else{echo "ابحث عن نطاق";}?>" name="srch-term" id="srch-term">
                                                <button class="btn btn-default domain-check" type="submit"><?php if (get_locale() == 'en_US') { echo "search";}else{echo "بحث";}?></button>
                                            </div>
                                          </form>
                                    </div>
                                </div>
                                <div class="col-md-9 col-sm-12 col-xs-12">
                                    <div class="domain-type"  data-aos="fade-down" data-aos-delay="200" data-aos-once="true">
                                    <?php if( have_rows('domains_block_price') ): while ( have_rows('domains_block_price') ) : the_row();?>
                                        <div class="tld-price">
                                            <span>
                                            <span class="tld"><?php the_sub_field('domain');?> </span> <span class="equal">=</span><span class="price"><?php the_sub_field('price');?> </span></span>
                                        </div>
                                    <?php endwhile;endif;?>
                                    </div>
                                    <div class="domain-result" style="display:none"  data-aos="fade-down" data-aos-delay="200" data-aos-once="true">
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                    <td><h5 class="d-name"></h5></td>
                                                    <td><h5 class="d-price"></h5></td>
                                                    <td><h5 class="d-status"></h5></td>
                                                    <td class="d-btn"><a href="#" class="btn btn-success"></a></td>
                                                </tr>
                                            </thead>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                 </div>
             </div>
         </section>
<?php endif;?>
         <!-- ======================why============================ -->
<?php if( get_field('active_why_block') ):?>
         <section class="why">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="main-title" data-aos="fade-down" data-aos-once="true">
                            <div class="title-group">
                                <?php echo the_field('why_block_head');?> 
                            </div>
                        </div>
                    </div>
                </div> 
                <div class="why-slider">

                <?php if( have_rows('why_block_slides') ): while ( have_rows('why_block_slides') ) : the_row();?>
                    <div class="why-content">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="why-img">
                                        <img class="img-responsive" data-animation="<?php if (get_locale() == 'en_US') { echo"fadeInLeft";}else{echo"fadeInRight";}?>" data-delay="0.5s" src="<?php the_sub_field('image');?>"/>
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-6 col-xs-12">
                                    <div class="why-text" data-animation="fadeInDown" data-delay="0.5s">
                                        <h4><?php the_sub_field('title');?></h4>
                                        <p><?php the_sub_field('description');?></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endwhile;endif;?>
                </div>
            </div>
         </section>
<?php endif;?>
         <!-- ======================details============================ -->
<?php if( get_field('active_details_block') ):?>
         <section class="details">
             <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="main-title"  data-aos="fade-down" data-aos-once="true">
                            <div class="title-group">
                                <?php echo the_field('details_block_head');?> 
                            </div>
                        </div>
                    </div>
                </div>
                 <div class="row">
                     <div class="col-md-12 text-center" data-aos="fade-down" data-aos-delay="200" data-aos-once="true">
                        <div class="detail-text" >
                          <?php echo the_field('details_block_data');?> 
                        </div>
                         <?php if(get_field('details_block_url')):?>
                          <a href="<?php echo the_field('details_block_url');?>" class="btn btn-default active more" role="button"> <?php _e('more','mdwp');?> </a>
                         <?php endif;?>
                     </div>
                 </div>
             </div>
         </section>
<?php endif;?>
          <!-- ======================counter============================ -->
<?php if( get_field('active_counters_block') ):$counters = get_field('counters_block');?>
         <section class="counters text-center">
             <div class="overlay"></div>
             <div class="container">
                 <div class="row" data-aos="fade-down" data-aos-once="true" data-aos-delay="400">
                    <div class="col-md-1"></div>
                    <div class=" col-md-2 col-sm-6 col-xs-6" data-aos="fade-down" data-aos-once="true" data-aos-delay="500">
                        <div class="counter-img grow">
                            <i class="fas fa-user fa-2x"></i>
                        </div>
                        <div class="counter-desc">
                            <h4><span class="counter-num"><?php echo $counters['employees'];?></span></h4>
                            <span><?php _e('Employees','mdwp');?></span>
                        </div>
                    </div>
                     <div class="col-md-2 col-sm-6 col-xs-6" data-aos="fade-down" data-aos-once="true" data-aos-delay="600">
                        <div class="counter-img grow">
                            <i class="far fa-handshake fa-2x"></i>
                        </div>
                        <div class="counter-desc">
                            <h4><span class="counter-num"><?php echo $counters['clients'];?></span></h4>
                            <span><?php _e('Clients','mdwp');?></span>
                        </div> 
                     </div>
                     <div class="col-md-2 col-sm-12 col-xs-12" data-aos="fade-down" data-aos-once="true" data-aos-delay="700">
                        <div class="counter-img grow">
                            <i class="far fa-calendar-alt fa-2x"></i>
                        </div>
                        <div class="counter-desc">
                            <h4><span class="counter-num"><?php echo $counters['experience'];?></span></h4>
                            <span><?php _e('Years of experience','mdwp');?></span>
                        </div> 
                     </div>
                     <div class="col-md-2 col-sm-6 col-xs-6" data-aos="fade-down" data-aos-once="true" data-aos-delay="800">
                        <div class="counter-img grow">
                            <i class="fas fa-building fa-2x"></i>
                        </div>
                        <div class="counter-desc">
                            <h4><span class="counter-num"><?php echo $counters['offices'];?></span></h4>
                            <span><?php _e('Offices','mdwp');?></span>
                         </div> 
                     </div>
                     <div class="col-md-2 col-sm-6 col-xs-6" data-aos="fade-down" data-aos-once="true" data-aos-delay="900">
                        <div class="counter-img grow">
                            <i class="fas fa-globe fa-2x"></i>
                        </div>
                         <div class="counter-desc">
                            <h4><span class="counter-num"><?php echo $counters['saudi_foundation'];?></span>%</h4>
                            <span><?php _e('Saudi Foundation','mdwp');?></span>
                         </div>
                     </div>
                 </div>
             </div>
         </section>
<?php endif;?>
          <!-- ======================services============================ -->
<?php if( get_field('active_hosting_block') ):?>
         <section class="services">
             <div class="container">
                 <div class="row">
                    <div class="col-md-12">
                        <div class="main-title" data-aos="fade-down" data-aos-once="true">
                            <div class="title-group">
                                <?php echo the_field('hosting_block_head');?> 
                            </div>
                        </div>
                     </div>
                 </div>
                 <div class="row">
                    <div class="col-md-12">
                        <div class="service-content text-center">
                          <?php if( have_rows('hosting_block_items') ): while ( have_rows('hosting_block_items') ) : the_row();?>
                            <div class="col-md-3 col-sm-6 col-xs-6">
                                <div class="service" data-aos="fade-down" data-aos-once="true" data-aos-delay="400">
                                    <div class="img-container" >
                                        <img src="<?php the_sub_field('icon');?>"/>
                                        <div class="img-desc">
                                            <h5><?php the_sub_field('title');?></h5>
                                           <p><?php the_sub_field('description');?></p>
                                        </div>
                                        <div class="more_service">
                                          <?php if(!empty(get_sub_field('url'))):?>
                                            <a  href="<?php the_sub_field('url');?>" class="btn button primary"><?php _e('more','mdwp');?> </a>
                                          <?php endif;?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                          <?php endwhile;endif;?>
                        </div>
                    </div>
                 </div>
             </div>
         </section>
<?php endif;?>
         <!-- ======================Clintes============================ -->
<?php if( get_field('active_clients_block') ):?>
         <section class="clintes">
            <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="step-title text-center" data-aos="fade-down" data-aos-once="true">
                        <?php echo the_field('clients_block_head');?> 
                    </div>
                </div>
            </div>
             <div class="client-slider">
               <?php $clients_imgs = get_field('clients_imgs');
                    if(is_array($clients_imgs)):foreach($clients_imgs as $client):?>
                 <div class="client-img">
                    <img src="<?php echo $client["url"];?>"/>
                 </div>
               <?php endforeach;endif;?>
             </div>
         </div>
         </section>
<?php endif;?>
         <!-- ======================technical services======================== -->
<?php if( get_field('active_tech_services') ):?>
         <section class="tech_services">
             <div class="container" data-aos="fade-down" data-aos-once="true" data-aos-delay="400">
                 <div class="row">
                    <div class="col-md-12">
                        <div class="main-title" data-aos="fade-down" data-aos-once="true">
                            <div class="title-group">
                                <h2><?php echo the_field('tech_services_t1');?> </h2>
                                <h3><?php echo the_field('tech_services_t2');?> </h3>
                            </div>
                        </div>
                     </div>
                 </div>
                 <div class="row" data-aos="fade-down" data-aos-once="true" data-aos-delay="500">
                    <div class="col-md-12">
                        <p class="tech-text"><?php echo the_field('tech_services_desc');?> </p>
                    </div>
                 </div>
                 <div class="row mb-50" data-aos="fade-down" data-aos-once="true" data-aos-delay="600">
                    <div class="col-md-6 col-sm-6 col-xs-6">
                        <span class="title"><?php echo the_field('tech_services_t3');?></span>
                        <span class="tel"><?php echo the_field('tech_services_t4');?></span>
                    </div>
                     <div class="col-md-6 col-sm-6 col-xs-6">
                         <span class="title"><?php echo the_field('tech_services_t5');?></span>
                         <span class="tel"><?php echo the_field('tech_services_t6');?></span>
                     </div>
                 </div>
                 <div class="row" data-aos="fade-down" data-aos-once="true" data-aos-delay="700">
                     <div class="contact text-center">
                        <div class="col-md-3 col-sm-6 col-xs-6">
                            <a href="<?php echo the_field('phone_url');?>"><div class="contact_content phone">
                                 <div class="contact_img">
                                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/te.png"/>
                                 </div>
                                 <a href="<?php echo the_field('phone_url');?>" class="contact-title"><?php echo the_field('phone');?></a>
                             </div>
                            </a>
                         </div>
                         <div class="col-md-3 col-sm-6 col-xs-6">
                             <a href="<?php echo the_field('mail_url');?>">
                                 <div class="contact_content mail">
                                     <div class="contact_img">
                                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/mail.png"/>
                                     </div>
                                     <a href="<?php echo the_field('mail_url');?>" class="contact-title"><?php echo the_field('mail');?> </a>
                                </div>
                             </a>
                         </div>
                         <div class="col-md-3 col-sm-6 col-xs-6">
                            <a href="<?php echo the_field('ticket_url');?>"> 
                                <div class="contact_content ticket">
                                     <div class="contact_img">
                                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/ticket.png"/>
                                     </div>
                                     <a href="<?php echo the_field('ticket_url');?>" class="contact-title"><?php echo the_field('ticket');?> </a>
                                </div>
                            </a>
                         </div>
                         <div class="col-md-3 col-sm-6 col-xs-6">
                            <a href="javascript: void(0);" onclick="hb_footprint.startChat();return false;">
                                 <div class="contact_content chat">
                                     <div class="contact_img">
                                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/chat.png"/>
                                     </div>
                                     <div id="hbinvitationcontainer_1ae3fa8ba55605f2"></div>
                                     <a class="contact-title" id="hbtagcontainer_1ae3fa8ba55605f2" href="javascript: void(0);" onclick="hb_footprint.startChat();return false;"><?php echo the_field('chat');?></a>
                                     <script type="text/javascript">var hb_script_tag_1ae3fa8ba55605f2=document.createElement("script");hb_script_tag_1ae3fa8ba55605f2.type="text/javascript";setTimeout('hb_script_tag_1ae3fa8ba55605f2.src="https://my.servarena.com/index.php?cmd=hbchat&action=embed&v=cmFuZGlkPTFhZTNmYThiYTU1NjA1ZjImaW52aXRlX2lkPTMmdGFnPWxpbmsmbGlua19jb250ZW50PUNsaWNrIGhlcmUgdG8gY2hhdCZwcm90b2NvbD1odHRwcyUzQSUyRiUyRg==";document.getElementById("hbtagcontainer_1ae3fa8ba55605f2").appendChild(hb_script_tag_1ae3fa8ba55605f2);',5);</script>
                                </div>
                             </a>
                         </div>
                     </div>
                 </div>
             </div>
         </section>
<?php endif;?>
<?php get_footer(); ?>
