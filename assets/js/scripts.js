$(window).on("scroll", function() {
    if($(window).scrollTop() > 50) {
        $(".navbar-default").addClass("shrink");
    } else {
        //remove the background property so it comes transparent again (defined in your css)
       $(".navbar-default").removeClass("shrink");
    }
       if($(window).scrollTop() > 0) {
        $(".nav-inside").addClass("shrink");
    } else {
        //remove the background property so it comes transparent again (defined in your css)
       $(".nav-inside").removeClass("shrink");
    }
     if($(window).fadeIn( "slow" ).scrollTop() > 50) {
        $(".chat-icon").addClass("chat-pos");
    } else {
        //remove the background property so it comes transparent again (defined in your css)
       $(".chat-icon").removeClass("chat-pos");
    }
    
/*
  if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
    document.getElementsByClassName("dropdown").style.bottom = "5px";
  } 
*/

});
$(document).ready(function(){
    //counter

$('.counter-num').counterUp({
    delay: 10,
    time: 1000
});

$(".navbar").on("show.bs.collapse", function(e) {
  console.log("open")
	$(".navbar-default").css("background-color", "#0c1e45");
});
$(".navbar").on("hide.bs.collapse", function(e) {
  console.log("close")
	$(".navbar-default").css("background-color", "transparent");
});

if (jQuery().tooltip) {$('[data-toggle="tooltip"]').tooltip();}
if (jQuery().niceScroll) {if ($(window).width() < 768) {$(".navbar-collapse").niceScroll();}}

$('.main_slider').on('init', function(e, slick) {
        var $firstAnimatingElements = $('div.item:first-child').find('[data-animation]');
        doAnimations($firstAnimatingElements);    
    });
$('.main_slider').on('beforeChange', function(e, slick, currentSlide, nextSlide) {
              var $animatingElements = $('div.item[data-slick-index="' + nextSlide + '"]').find('[data-animation]');
              doAnimations($animatingElements);    
    });

$('.why-slider').on('init', function(e, slick) {
        var $firstAnimatingElements = $('div.why-content:first-child').find('[data-animation]');
        doAnimations($firstAnimatingElements);    
    });
$('.why-slider').on('beforeChange', function(e, slick, currentSlide, nextSlide) {
              var $animatingElements = $('div.why-content[data-slick-index="' + nextSlide + '"]').find('[data-animation]');
              doAnimations($animatingElements);    
    });
    
$('.work_slider').on('init', function(e, slick) {
        var $firstAnimatingElements = $('div.item:first-child').find('[data-animation]');
        doAnimations($firstAnimatingElements);    
    });
$('.work_slider').on('beforeChange', function(e, slick, currentSlide, nextSlide) {
              var $animatingElements = $('div.item[data-slick-index="' + nextSlide + '"]').find('[data-animation]');
              doAnimations($animatingElements);    
    });

$('.server_feature .servers-icon').on('init', function(e, slick) {
        var $firstAnimatingElements = $('div.slide_container:first-child').find('[data-animation]');
        doAnimations($firstAnimatingElements);    
    });
$('.server_feature .servers-icon').on('beforeChange', function(e, slick, currentSlide, nextSlide) {
              var $animatingElements = $('div.slide_container[data-slick-index="' + nextSlide + '"]').find('[data-animation]');
              doAnimations($animatingElements);    
    });
  
if (jQuery().slick) {
$('.main_slider').slick({
      rtl:rtlcheck,
      slidesToShow: 1,
      autoplay: true,
      autoplaySpeed: 5000,
      dots: true,
      arrows: false,
      infinite: true,
      speed: 300,
//      fade: true,
    });
$('.server_feature .servers-icon').slick({
    rtl:rtlcheck,
    slidesToShow: 1,
    autoplay: false,
    //autoplaySpeed: 5000,
    dots: false,
    arrows: true,
    infinite: true,
    speed: 300,
    fade: true,
    focusOnSelect: false,
    draggable: false,
    cssEase: 'ease',
    useCSS: true,
    useTransform: true,
    waitForAnimate: true,
    touchThreshold: 100,
    prevArrow: '<button type="button" class="slick-prev">Previous</button>',
    nextArrow: '<button type="button" class="slick-next">Next</button>'
    });
$('.client-slider').slick({
    rtl:rtlcheck,
    centerMode: true,
    centerPadding: '25px',
    slidesToShow: 4,
    arrows: false,
    dots: false,
    autoplay: true,
    autoplaySpeed: 5000,
    speed: 3000,
    focusOnSelect: false,
    draggable: true,
    infinite: true,

    responsive: [
    {
      breakpoint: 768,
      settings: {
        autoplay: true,
        autoplaySpeed: 5000,
        dots: false,
        arrows: false,
        infinite: true,
        speed: 3000,
        centerMode: true,
        centerPadding: '20px',
        slidesToShow: 2,
        focusOnSelect: false,
      }
    },
    {
      breakpoint: 480,
      settings: {
        autoplay: true,
        autoplaySpeed: 5000,
        dots: false,
        arrows: false,
        infinite: true,
        speed: 3000,
        centerMode: true,
        centerPadding: '20px',
        slidesToShow: 1,
        focusOnSelect: false,
      }
    }
  ]
    });
$('.why-slider').slick({
    rtl:rtlcheck,
    slidesToShow: 1,
    autoplay: true,
    autoplaySpeed: 5000,
    dots: false,
    arrows: true,
    infinite: true,
    speed: 300,
    fade: true,
    focusOnSelect: false,
    draggable: false,
    cssEase: 'ease',
    useCSS: true,
    useTransform: true,
    waitForAnimate: true,
    touchThreshold: 100,
    });
$('.inside_slider').slick({
      rtl:rtlcheck,
      slidesToShow: 1,
      autoplay: true,
      autoplaySpeed: 5000,
      arrows: false,
      infinite: true,
      speed: 300,
//      fade: true,
    responsive: [
    {
      breakpoint: 768,
      settings: {
        autoplay: true,
        autoplaySpeed: 5000,
        dots: false,
        arrows: false,
        infinite: true,
        speed: 300,
        focusOnSelect: false,
      }
    },
    {
      breakpoint: 480,
      settings: {
        autoplay: true,
        autoplaySpeed: 5000,
        dots: false,
        arrows: false,
        infinite: true,
        speed: 300,
        slidesToShow: 1,
        focusOnSelect: false,
      }
    }
  ]
    }); 
$('.work_slider').slick({
    rtl:rtlcheck,
    slidesToShow: 1,
    autoplay: true,
    autoplaySpeed: 5000,
    dots: true,
    arrows: false,
    infinite: true,
    speed: 300,
    fade: true,
    focusOnSelect: false,
    draggable: false,
    cssEase: 'ease',
    useCSS: true,
    useTransform: true,
    waitForAnimate: true,
    touchThreshold: 100,
    });
}
if ($('#map').length > 0) {
function initialize() {
var markers = [];
var activeIcon = {
    url: 'https://new.servarena.com/wp-content/themes/servarenarep/assets/img/active.png'
};
var normalIcon = {
  url: 'https://new.servarena.com/wp-content/themes/servarenarep/assets/img/normal.png'
  };
var locations = [
     ["<div class='hidden-xs' style='float:left'><img src='https://new.servarena.com/wp-content/themes/servarenarep/assets/img/ksa.png' style='margin-left: 25px></div>"+
      "<div style='float:right; padding: 10px;direction: ltr;'><b><p  style='font-size:18px;padding-left: 23px;'>KSA<span  style='font-size:12px;'>.Jeddah</span></p></b>"+
      "<ul style='font-size:10px;'><li> Hunebedstraat 6A, 5342 LK, Oss, Noord-Brabant</li><li> 003106199   59535 </li><li>00310412799800</li></ul></div>"
      + "<div><b>"+ "</b></div>", 21.575764,39.191476, 4 ],
      ["<div class='hidden-xs' style='float:left; color: #4e596b;'><img src='https://new.servarena.com/wp-content/themes/servarenarep/assets/img/uae.png' style='margin-left: 25px></div>"+
      "<div style='float:right; padding: 10px;direction: ltr;'><b><p  style='font-size:18px;padding-left: 23px;'>UAE</p></b>" +
        "<ul style='font-size:10px;'><li> Hunebedstraat 6A, 5342 LK, Oss, Noord-Brabant</li><li> 003106199   59535 </li><li>00310412799800</li></ul></div>"
      + "<div><b>"+ "</b></div>", 25.069283,55.141722, 5],
      ["<div class='hidden-xs' style='float:left'><img src='https://new.servarena.com/wp-content/themes/servarenarep/assets/img/netherland.png' style='margin-left: 25px></div>"+
      "<div style='float:right; padding: 10px;direction: ltr;'><b><p  style='font-size:18px;padding-left: 23px;'>Netherlands</p></b>"+ 
        "<ul style='font-size:10px;'><li> Hunebedstraat 6A, 5342 LK, Oss, Noord-Brabant</li><li> 003106199   59535 </li><li>00310412799800</li></ul></div>"
      + "<div><b>"+ "</b></div>", 51.739024,5.558297, 3],
      ["<div class='hidden-xs' style='float:left'><img src='https://new.servarena.com/wp-content/themes/servarenarep/assets/img/egypt1.png' style='margin-left: 25px></div>"+
      "<div style='float:right; padding: 10px;direction: ltr;'><b><p  style='font-size:18px;padding-left: 23px;'>Egypt</p></b>"+
        "<ul style='font-size:10px;'><li> Hunebedstraat 6A, 5342 LK, Oss, Noord-Brabant</li><li> 003106199   59535 </li><li>00310412799800</li></ul></div>"
      + "<div><b>"+ "</b></div>", 29.960157,31.248483, 2],
      ["<div class='hidden-xs' style='float:left'><img src='https://new.servarena.com/wp-content/themes/servarenarep/assets/img/bahrain1.png' style='margin-left: 25px></div>"+
      "<div style='float:right; padding: 10px;direction: ltr;'><b><p  style='font-size:18px;padding-left: 23px'>Bahrain</p></b>" +
        "<ul style='font-size:10px;'><li> Hunebedstraat 6A, 5342 LK, Oss, Noord-Brabant</li><li> 003106199   59535 </li><li>00310412799800</li></ul></div>"
      + "<div><b>"+ "</b></div>" , 26.240070,50.587345, 1]
    ];
var mapOptions = {
    zoom: 2,
    center: new google.maps.LatLng(61.324265,1.441182),
    styles: [
              {elementType: 'geometry', stylers: [{color: '#2e3340'}]},
              {elementType: 'labels.text.fill', stylers: [{color: '#2e3340'}]},
              {elementType: 'labels.text.stroke', stylers: [{color: '#2e3340'}]},
              {
                featureType: 'administrative',
                elementType: 'geometry.stroke',
                stylers: [{color: '#2e3340'}]
              },
              {
                featureType: 'administrative.land_parcel',
                elementType: 'geometry.stroke',
                stylers: [{color: '#2e3340'}]
              },
              {
                featureType: 'administrative.land_parcel',
                elementType: 'labels.text.fill',
                stylers: [{color: '#2e3340'}]
              },
              {
                featureType: 'landscape.natural',
                elementType: 'geometry',
                stylers: [{color: '#2e3340'}]
              },
              {
                featureType: 'poi',
                elementType: 'geometry',
                stylers: [{color: '#2e3340'}]
              },
              {
                featureType: 'poi',
                elementType: 'labels.text.fill',
                stylers: [{color: '#2e3340'}]
              },
              {
                featureType: 'poi.park',
                elementType: 'geometry.fill',
                stylers: [{color: '#2e3340'}]
              },
              {
                featureType: 'poi.park',
                elementType: 'labels.text.fill',
                stylers: [{color: '#363b48'}]
              },
              {
                featureType: 'road',
                elementType: 'geometry',
                stylers: [{color: '#262b38'}]
              },
              {
                featureType: 'road.arterial',
                elementType: 'geometry',
                stylers: [{color: '#262b38'}]
              },
              {
                featureType: 'road.highway',
                elementType: 'geometry',
                stylers: [{color: '#262b38'}]
              },
              {
                featureType: 'road.highway',
                elementType: 'geometry.stroke',
                stylers: [{color: '#262b38'}]
              },
              {
                featureType: 'road.highway.controlled_access',
                elementType: 'geometry',
                stylers: [{color: '#262b38'}]
              },
              {
                featureType: 'road.highway.controlled_access',
                elementType: 'geometry.stroke',
                stylers: [{color: '#262b38'}]
              },
              {
                featureType: 'road.local',
                elementType: 'labels.text.fill',
                stylers: [{color: '#262b38'}]
              },
              {
                featureType: 'transit.line',
                elementType: 'geometry',
                stylers: [{color: '#2e3340'}]
              },
              {
                featureType: 'transit.line',
                elementType: 'labels.text.fill',
                stylers: [{color: '#8f7d77'}]
              },
              {
                featureType: 'transit.line',
                elementType: 'labels.text.stroke',
                stylers: [{color: '#363b48'}]
              },
              {
                featureType: 'transit.station',
                elementType: 'geometry',
                stylers: [{color: '#2e3340'}]
              },
              {
                featureType: 'water',
                elementType: 'geometry.fill',
                stylers: [{color: '#262b38'}]
              },
              {
                featureType: 'water',
                elementType: 'labels.text.fill',
                stylers: [{color: '#92998d'}]
              }
            ],
    mapTypeId: google.maps.MapTypeId.ROADMAP
  };
map = new google.maps.Map(document.getElementById("map"), mapOptions);
var infowindow = new google.maps.InfoWindow();
infowindow.setZIndex(2);
//  infowindow.style.borderColor="black";
var marker, i;
for (i = 0; i < locations.length; i++) {
    marker = new google.maps.Marker({
      position: new google.maps.LatLng(locations[i][1], locations[i][2]),
      map: map,
      icon: "http://d.up-00.com/2018/07/153285439947241.png",
    });
    markers.push(marker);
    google.maps.event.addListener(marker, 'click', (function(marker, i) {
      return function() {
        //console.log(activeIcon);
        for (var j = 0; j < markers.length; j++) {
          markers[j].setIcon(normalIcon);
        }
        infowindow.setContent(locations[i][0]);
        this.setIcon(activeIcon);
//        var popupString =  "<div style='float:left'><img src='http://i.stack.imgur.com/g672i.png'></div>"+
//            "<div style='float:right; padding: 10px;'><b>Title</b><br/>123 Address<br/> City,Country</div>"
//+'<div><b>' + locations[i][0] + '</b></div>'

//            infowindow.setContent(popupString);
        infowindow.open(map, marker);
      }
    })(marker, i));
  }
google.maps.event.addListener(infowindow, 'domready', function() {

    // Reference to the DIV that wraps the bottom of infowindow
    var iwOuter = $('.gm-style-iw');
    var iwimg =$('.gm-style img');
    /* Since this div is in a position prior to .gm-div style-iw.
     * We use jQuery and create a iwBackground variable,
     * and took advantage of the existing reference .gm-style-iw for the previous div with .prev().
    */
    var iwBackground = iwOuter.prev();
    var iwparent= iwBackground.parent();
    iwOuter.css({'background-color': '#fff'});
    iwOuter.css({top: '30px'});
    iwOuter.css({'box-shadow': 'rgba(255, 255, 255, 0.15) 0px 0px 0px 4px'});
//    iwOuter.append('<style>iwOuter::before{box-shadow: rgba(255, 255, 255, 0.15) 0px 0px 0px 4px}</style>');
    iwBackground.children(':nth-child(2)').css({'box-shadow' : 'none'});
    iwBackground.children(':nth-child(2)').css({'background-color' : 'transparent'});
    iwBackground.children(':nth-child(3)').css({'transform' : 'translateY(6px)'});
    iwBackground.children(':nth-child(4)').css({'background-color' : 'transparent'});
    iwOuter.children(':nth-child(1)').css({'transform' : 'translateX(-25px)'});
    iwOuter.children(':nth-child(1)').children(':nth-child(1)').find(iwimg).css({'height': '100px'});
    var iwCloseBtn = iwOuter.next();
    
    // Apply the desired effect to the close button
    iwCloseBtn.css({opacity: '1', right: '30px', top: '40px'});
    iwCloseBtn.mouseout(function(){
      $(this).css({opacity: '1'});
  });
})
}
google.maps.event.addDomListener(window, 'load', initialize);
}
function doAnimations(elements) {
        var animationEndEvents = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';
        elements.each(function() {
            var $this = $(this);
            var $animationDelay = $this.data('delay');
            var $animationType = 'animated ' + $this.data('animation');
            $this.css({
                'animation-delay': $animationDelay,
                '-webkit-animation-delay': $animationDelay
            });
            $this.addClass($animationType).one(animationEndEvents, function() {
                $this.removeClass($animationType);
            });
        });
    };

//FAQ
  $('.panel-collapse').on('show.bs.collapse', function () {
    $(this).siblings('.panel-heading').addClass('active');
  });

  $('.panel-collapse').on('hide.bs.collapse', function () {
    $(this).siblings('.panel-heading').removeClass('active');
  });

if ($('.step_bulid_app').length > 0) {
     var width = (window.innerWidth > 0) ? window.innerWidth : screen.width;
     var controller = new ScrollMagic.Controller();
    if(width > 991) {
var tween = new TimelineMax()
.fromTo(".first_circle", 1,
    {top: '37em',left: '56em'},
    {top: '29em',left:'51em',  yoyo: true, ease: Circ.easeInOut},1
)
.fromTo(".second_circle", 1,
    {top: '29em',left:'51em'},
    {top: '46em',left:'46em', yoyo: true, ease: Circ.easeInOut},1
)
.fromTo(".third_circle", 1,
    {top: '46em',left:'46em'},
    {top: '29em',left:'39em',  yoyo: true, ease: Circ.easeInOut},1

    )
.fromTo(".last_circle", 1,
    {top: '29em',left:'39em'},
    {top:  '36.5em',left:'32em', yoyo: true, ease: Circ.easeInOut},1
    )
.fromTo(".final_circle", 1,
    {top: '33em',left:'20em'},
    {top: '33em',left:'15em', yoyo: true, ease: Circ.easeInOut},1
    )
.fromTo(".mobile_app", 1,
    {top: '30em',left: '15em'},
    {top: '30em',left: '10em', yoyo: true, ease: Circ.easeInOut},1
    );
 }
    if(width < 991) {
       var tween = new TimelineMax()
    .fromTo(".first_circle", 1,
        {top: '36.5em',left: '46em'},
        {top: '40em',left:'36em',  yoyo: true, ease: Circ.easeInOut},1
    )
    .fromTo(".second_circle", 1,
        {top: '40em',left:'36em'},
        {top: '48em',left:'56em', yoyo: true, ease: Circ.easeInOut},1
    )
    .fromTo(".third_circle", 1,
        {top: '48em',left:'56em'},
        {top: '56em',left:'36em',  yoyo: true, ease: Circ.easeInOut},1

        )
    .fromTo(".last_circle", 1,
        {top: '56em',left:'36em'},
        {top:  '62em',left:'45em', yoyo: true, ease: Circ.easeInOut},1
        )
    .fromTo(".final_circle", 1,
        {top: '70em',left:'41.5em'},
        {top: '68em',left:'41.5em', yoyo: true, ease: Circ.easeInOut},1
        )
    .fromTo(".mobile_app", 1,
        {top: '68em',left: '37em'},
        {top: '66em',left: '37em', yoyo: true, ease: Circ.easeInOut},1
        ); 
    }
    if(width < 767) {
         var tween = new TimelineMax()
        .fromTo(".first_circle", 1,
        {top: '29em',left: '34em'},
        {top: '32em',left:'29em',  yoyo: true, ease: Circ.easeInOut},1
    )
    .fromTo(".second_circle", 1,
        {top: '32em',left:'29em'},
        {top: '35em',left:'38.5em', yoyo: true, ease: Circ.easeInOut},1
    )
    .fromTo(".third_circle", 1,
        {top: '35em',left:'38.5em'},
        {top: '39.5em',left:'28.5em',  yoyo: true, ease: Circ.easeInOut},1

        )
    .fromTo(".last_circle", 1,
        {top: '39.5em',left:'28.5em'},
        {top: '43em',left:'33.3em', yoyo: true, ease: Circ.easeInOut},1
        )
    .fromTo(".final_circle", 1,
        {top: '47.5em',left:'31.5em'},
        {top: '48em',left:'31.5em', yoyo: true, ease: Circ.easeInOut},1
        )
    .fromTo(".mobile_app", 1,
        {top: '47em',left: '29em'},
        {top: '48em',left: '29em', yoyo: true, ease: Circ.easeInOut},1
        ); 
     
// build scene
var scene = new ScrollMagic.Scene({triggerElement: ".end_trigger", duration: 150})
    .setTween(tween)
//    .addIndicators({name: "loop"}) // add indicators (requires plugin)
    .addTo(controller);
 }

// build scene
var scene = new ScrollMagic.Scene({triggerElement: ".end_trigger", duration: 150})
    .setTween(tween)
//    .addIndicators({name: "loop"}) // add indicators (requires plugin)
    .addTo(controller);
 }
if ($('.our_work').length > 0) {
// init controller
var controller = new ScrollMagic.Controller();
var width = (window.innerWidth > 0) ? window.innerWidth : screen.width;
// trigger a velocity opaticy animation

// build scene
var scene = new ScrollMagic.Scene({triggerElement: "#trigger"})
.setVelocity(".smart_phone6", {opacity: 1}, {duration: 200})
.addTo(controller);
$.Velocity.defaults.mobileHA = false;
    

}
if ($('.our_work').length > 0) {
// init controller
var controller = new ScrollMagic.Controller
var width = (window.innerWidth > 0) ? window.innerWidth : screen.width;
// trigger a velocity opaticy animation

// build scene
var scene = new ScrollMagic.Scene({triggerElement: "#trigger"})
// trigger a velocity opaticy animation
.setVelocity(".smart_phone7", {opacity: 1}, {duration: 700})
.addTo(controller);
     $.Velocity.defaults.mobileHA = false;
}
if ($('.our_work').length > 0) {
// init controller
var controller = new ScrollMagic.Controller();
var width = (window.innerWidth > 0) ? window.innerWidth : screen.width;
// trigger a velocity opaticy animation

// build scene
var scene = new ScrollMagic.Scene({triggerElement: "#trigger"})
// trigger a velocity opaticy animation
.setVelocity(".smart_phone8", {opacity: 1}, {duration: 1200})
.addTo(controller);
    $.Velocity.defaults.mobileHA = false;
}
if ($('.our_work').length > 0) {
// init controller
var controller = new ScrollMagic.Controller();
var width = (window.innerWidth > 0) ? window.innerWidth : screen.width;
// trigger a velocity opaticy animation
// build scene
var scene = new ScrollMagic.Scene({triggerElement: "#trigger"})
// trigger a velocity opaticy animation
.setVelocity(".smart_phone5", {opacity: 1}, {duration: 1700})
.addTo(controller);
    $.Velocity.defaults.mobileHA = false;
} 
if ($('.our_work').length > 0) {
// init controller
var controller = new ScrollMagic.Controller();
var width = (window.innerWidth > 0) ? window.innerWidth : screen.width;
// trigger a velocity opaticy animation

// build scene
var scene = new ScrollMagic.Scene({triggerElement: "#trigger"})
// trigger a velocity opaticy animation
.setVelocity(".smart_phone2", {opacity: 1}, {duration: 2200})
.addTo(controller);
    $.Velocity.defaults.mobileHA = false;
}
if ($('.our_work').length > 0) {
// init controller
var controller = new ScrollMagic.Controller();
var width = (window.innerWidth > 0) ? window.innerWidth : screen.width;
// trigger a velocity opaticy animation

// build scene
var scene = new ScrollMagic.Scene({triggerElement: "#trigger"})
// trigger a velocity opaticy animation
.setVelocity(".smart_phone1", {opacity: 1}, {duration: 2700})
.addTo(controller);

} 
if ($('.our_work').length > 0) {
// init controller
var controller = new ScrollMagic.Controller();
var width = (window.innerWidth > 0) ? window.innerWidth : screen.width;
// trigger a velocity opaticy animation

// build scene
var scene = new ScrollMagic.Scene({triggerElement: "#trigger"})
// trigger a velocity opaticy animation
.setVelocity(".smart_phone4", {opacity: 1}, {duration: 3200})
.addTo(controller);
    $.Velocity.defaults.mobileHA = false;

} 
if ($('.our_work').length > 0) {
    // init controller
    var controller = new ScrollMagic.Controller();
    var width = (window.innerWidth > 0) ? window.innerWidth : screen.width;
// trigger a velocity opaticy animation

    // build scene
    var scene = new ScrollMagic.Scene({triggerElement: "#trigger"})
    // trigger a velocity opaticy animation
    .setVelocity(".smart_phone3", {opacity: 1}, {duration: 3700})
    .addTo(controller);
$.Velocity.defaults.mobileHA = false;
 } 
if ($('.about').length > 0) {
   AOS.init(); 
}


//$(document).ready(function(){

  // Plans hover
  $(".single-price").hover(function(){
    $(".single-price").removeClass("active");
    $(this).addClass("active");
  },function(){
    $(".single-price").removeClass("active");
    $(".single-price[data-active]").addClass("active");
  });

  // Domains Ajax
  $(".domain-check").click(function(){
    var getSld = $("#srch-term").val();
    var getExt = '.com' ;
    if(getSld.indexOf(".") > 1) {
      getSld = getSld.split(".");
      getExt = getSld[1];
      getSld = getSld[0];
    }
    $.ajax({
      method: "POST",
      url: "https://my.servarena.com/?/cart/v1/checkdomain",
      data: { cat_id: 4, sld: getSld, tld: getExt },
      beforeSend: function( xhr ) {
        console.log("start");
      }
    })
      .done(function( data ) {

        if(data.status == "ok"){
          var d_name = data.sld + data.tld ;
          var d_status = "Available";
          var d_price = data.prices[1].register;
          var d_period = data.prices[1].period;
          var d_btn = "Register";
        }else{
          var d_name = data.sld + data.tld ;
          var d_status = "Not Available";
          var d_price = data.prices[1].transfer;
          var d_period = data.prices[1].period;
          var d_btn = "Transfer";
        }
        $(".domain-type").hide();
        $(".domain-result").slideDown(200,function(){
          $(".d-name",this).text(d_name);
          $(".d-status",this).text(d_status);
          $(".d-price",this).text(d_price + "$ / " +d_period+" Year");
          $(".d-btn a",this).text(d_btn);
        });
      });
    return false
  });
//if (jQuery().parallax){ 
//    jQuery('.parallax-layer').parallax({
//				mouseport: jQuery(".slider")
//			});}
//if ($('.slider_insidepage').length > 0) {
//
//        var movementStrength = 40;
//         var height = movementStrength / $(window).height();
//         var width = movementStrength / $(window).width();
//         $(".container").mousemove(function(e) {
//           var pageX = e.pageX - ($(window).width() / 2);
//           var pageY = e.pageY - ($(window).height() / 2);
//           var newvalueX = width * pageX * -1;
//           var newvalueY = height * pageY * -1;
//           $(this).css("margin-left", newvalueX + "px");
//           $(this).css("margin-top", newvalueY + "px");
//         });
//}
 
    
    if ($('.slider_insidepage').length > 0) {    
var lFollowX = 0,
    lFollowY = 0,
    x = 0,
    y = 0,
    friction = 1 / 30;

function moveBackground() {
  x += (lFollowX - x) * friction;
  y += (lFollowY - y) * friction;
  
  translate = 'translate(' + x + 'px, ' + y + 'px) scale(1.1)';

  $('.slider').css({
    '-webit-transform': translate,
    '-moz-transform': translate,
    'transform': translate
  });

  window.requestAnimationFrame(moveBackground);
}

$(window).on('mousemove click', function(e) {

  var lMouseX = Math.max(-100, Math.min(100, $(window).width() / 2 - e.clientX));
  var lMouseY = Math.max(-100, Math.min(100, $(window).height() / 2 - e.clientY));
  lFollowX = (30 * lMouseX) / 100; // 100 : 12 = lMouxeX : lFollow
  lFollowY = (20 * lMouseY) / 100;

});

moveBackground();
    }
    

});
