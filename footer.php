         <section class="footer">
            <div class="container" data-aos="fade-down" data-aos-delay="200" data-aos-once="true">
                 <div class="row">
                     <div class="footer-desc">
                         <div class="col-md-6 col-sm-12 col-xs-12">
                             <div class="col-md-6 col-sm-6 col-xs-6 pr-0">
                                <div class="footer-content" data-aos="fade-down" data-aos-delay="400" data-aos-once="true">
                                  <?php dynamic_sidebar( 'widget-ft-1' ); ?>
                                </div>
                             </div>
                             <div class="col-md-6 col-sm-6 col-xs-6 pr-40">
                            <div class="footer-content" data-aos="fade-down" data-aos-delay="500" data-aos-once="true">
                                <?php dynamic_sidebar( 'widget-ft-2' ); ?>
                            </div>
                         </div>
                         </div>
                         <div class="col-md-6 col-sm-12 col-xs-12">
                             <div class="col-md-5 col-md-offset-1 col-sm-6 col-xs-6">
                                <div class="footer-content" data-aos="fade-down" data-aos-delay="600" data-aos-once="true">
                                    <?php dynamic_sidebar( 'widget-ft-3' ); ?>
                                </div>
                             </div>
                             <div class="col-md-4 col-md-offset-2 col-sm-6 col-xs-6">
                            <div class="footer-content" data-aos="fade-down" data-aos-delay="700" data-aos-once="true">
                                  <?php dynamic_sidebar( 'widget-ft-4' ); ?>
                            </div>
                         </div>
                         </div>
                         <div class="col-md-5 col-sm-6 col-xs-12 pull-left search-pos">
                            <div class="footer-search">
                                <?php if (get_locale() == 'en_US') {
                                    echo do_shortcode('[contact-form-7 id="420" title="Subscribe Form"]');
                                }else{
                                    echo do_shortcode('[contact-form-7 id="421" title="فورم المتابعه"]');
                                }?>
                            </div>
                        </div>
                     </div>
                 </div>
                <div class="row">
                    <div class="copy-right">
                         <div class="col-md-4 col-sm-12 col-xs-12 payment">
                             <span><?php if (get_locale() == 'en_US') { echo "<a href='https://www.servarena.com/en/payment-methods/'>Payment Methods</a>";}else{echo "<a href='https://www.servarena.com/%D9%88%D8%B3%D8%A7%D8%A6%D9%84-%D8%A7%D9%84%D8%AF%D9%81%D8%B9/'>طرق الدفع</a>";}?></span>
                             <img src="<?php echo get_template_directory_uri(); ?>/assets/img/payment_type.png"/>
                         </div>
                         <div class="col-md-4 col-sm-12 col-xs-12 icons">
                             <a href="https://www.behance.net/servarena" target="_blank" title="behance"><i class="fab fa-behance" ></i></a>
                             <a href="https://www.instagram.com/servarena/" target="_blank" title="instagram"><i class="fab fa-instagram"></i></a>
                             <a href="https://www.pinterest.com/servarena/" target="_blank" title="pinterest"><i class="fab fa-pinterest-p"></i></a>
                             <a href="https://www.linkedin.com/company/servarena/" target="_blank" title="linkedin"><i class="fab fa-linkedin-in"></i></a>
                             <a href="https://twitter.com/servarena" target="_blank" title="twitter"><i class="fab fa-twitter"></i></a>
                             <a href="https://www.facebook.com/Servarena1" target="_blank" title="facebook"><i class="fab fa-facebook-f"></i></a>
                         </div>
                        <div class="col-md-4 col-sm-12 col-xs-12 copy-right-text">
                            <p><?php if (get_locale() == 'en_US') { echo "Copyright © 2011 <a href='https://www.servarena.com/'>Servarena</a>. All rights reserved";}else{echo "جميع الحقوق محفوظه موقع <a href='https://www.servarena.com/'>ساحة الخدمات</a> © 2017";}?></p>
                         </div>
                     </div>
                </div>
             </div>
          </section>
         <!-- ======================chat======================== -->
         <!--
         <section class="chat-icon">
             <div class="tooltip">
                <img class="chat-img" src="<?php /*echo get_template_directory_uri();*/ ?>/assets/img/talk.png"/>
                 <span class="tooltiptext">تحدث لنا</span>
             </div>
         </section>
         -->
      </main>

    <script src="<?php echo get_template_directory_uri(); ?>/assets/js/jquery-1.11.0.min.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/assets/js/jquery-migrate-1.2.1.min.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/assets/js/bootstrap.min.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/assets/slick/slick.min.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/assets/js/jquery.nicescroll.min.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/assets/js/TweenMax.min.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/assets/js/velocity.min.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/assets/js/ScrollMagic.min.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/assets/js/animation.velocity.min.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/assets/js/animation.gsap.min.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/assets/js/tracking.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/assets/js/debug.addIndicators.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/assets/js/waypoints.min.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/assets/js/counterup.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/assets/js/aos.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/assets/js/scripts.js"></script>
    <script>
    $(document).ready(function(){
        AOS.init({
            duration: 1200,
        })   
    }); 
    </script>
		<?php wp_footer(); ?>
      <script type="text/javascript">
        <?php if(is_rtl()):?>
            var rtlcheck = true;
        <?php else:?>
            var rtlcheck = false;
        <?php endif;?>
      </script>
	</body>
</html>
