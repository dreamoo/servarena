<?php get_header(); ?>
	<main role="main" class="blog post-single">
	<section>
	<?php if (have_posts()): while (have_posts()) : the_post(); ?>

		<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

			<?php if ( has_post_thumbnail()) : ?>
				<div class="post-img">
					<?php the_post_thumbnail('full'); ?>
				</div>
			<?php endif; ?>

		  	<div class="post-content container">
				<h1><?php the_title(); ?></h1>
				<div class="post-details">
					<span class="date"><?php the_time('F j, Y'); ?> <?php the_time('g:i a'); ?></span>
				</div>
				<?php the_content();  ?>

				<hr />
				<?php comments_template(); ?>
		  	</div>

		</article>

	<?php endwhile; ?>

	<?php else: ?>

		<article>

			<h1><?php _e( 'Sorry, nothing to display.', 'mdwp' ); ?></h1>

		</article>

	<?php endif; ?>

	</section>


<section class="posts container">
<h3 class="section-title"><?php _e('Related Posts','mdwp');?></h3>
<?php $args = array(
	'post_type' => 'post',
	'orderby'   => 'rand',
	'posts_per_page' => 2,
	'post__not_in' => array(get_the_id()) 
	);
	$query = new WP_Query( $args );
 if ($query->have_posts()): while ($query->have_posts()) : $query->the_post();?>

	<article id="post-<?php the_ID(); ?>" <?php post_class('col-xs-12 col-md-6'); ?>>
		<div class="media" data-aos="fade-left" data-aos-delay="100" data-aos-once="true">
			<div class="media-left" data-aos="fade-left" data-aos-delay="400" data-aos-once="true">
				<!-- post thumbnail -->
				<?php if ( has_post_thumbnail()) : // Check if thumbnail exists ?>
					<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
						<?php the_post_thumbnail(array(120,120), array('class' => 'media-object')); // Declare pixel size you need inside the array ?>
					</a>
				<?php endif; ?>
				<!-- /post thumbnail -->
			</div>

			<div class="media-body" data-aos="fade-left" data-aos-delay="600"  data-aos-once="true">
				<h4 class="media-heading">
					<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a>
				</h4>
				<div class="post-details">
					<span class="category"><?php echo $cat_name;?></span> | 
					<span class="date"><?php the_time('F Y'); ?> </span>
				</div>
				<?php mdwpwp_excerpt('mdwpwp_index');?>

			</div>


		</div>
	</article>

	<?php endwhile;endif; ?>
</section>

	</main>

<?php get_sidebar(); ?>

<?php get_footer(); ?>
